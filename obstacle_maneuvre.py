from spline_math import *
from math import cos, sin
import numpy as np

class Control_params(object):    
    def __init__(self):
        self.fill_not_ok()
        
    def fill_not_ok(self):
        self.ok = False
        self.cfs = np.zeros((3,4))
        self.cfs_index = 0
        self.sstar = 0;
        self.pos_obsct = np.zeros((3,))
        self.p = np.zeros((3,))
        self.dp = np.zeros((3,))
        self.ddp = np.zeros((3,))
        self.Delta = np.zeros((3,))
        self.delta = 0
        self.obstc_dist = 100
        
    def fill_ok(self, s, cfs, index, y, obstc_dist):
        self.ok = True
        self.cfs = cfs
        self.cfs_index = index
        self.sstar = s;
        self.p = spline_point(cfs, s);
        self.dp = spline_point_derv(cfs, s);
        self.ddp = spline_point_double_derv(cfs, s);
        self.Delta = y - self.p;
        self.pos_obsct = y
        cr_pr_z = np.cross(self.p, self.Delta)[2] 
        if(cr_pr_z<0):
            sign = 1
        else:
            sign = -1
        self.delta = sign*np.linalg.norm(self.Delta);
        self.obstc_dist = obstc_dist
        
    def __lt__(self, other):
         return abs(self.obstc_dist) < abs(other.obstc_dist)
        
    def __le__(self, other):
        return abs(self.obstc_dist) <= abs(other.obstc_dist)

    def __eq__(self, other):
        return abs(self.obstc_dist) == abs(other.obstc_dist)

    def __ne__(self, other):
        return abs(self.obstc_dist) != abs(other.obstc_dist)

    def __gt__(self, other):
        return abs(self.obstc_dist) > abs(other.obstc_dist)

    def __ge__(self, other):
        return abs(self.obstc_dist) >= abs(other.obstc_dist)  
    
    def print_all(self):
        print(self.ok)
        print(f'pos obstcl: {self.pos_obsct}')
        print(f'ind: {self.cfs_index}')
        print(f's*: {self.sstar}')
        print(f'p: {self.p}')
        print(f'dp: {self.dp}')
        print(f'Delta: {self.Delta}')
        print(f'delta: {self.delta}')
        print(f'dist to obstc: {self.obstc_dist}')
    
def find_control_params(start_ind, max_dist, spline, y_obsctle):
    distance = 0
    ind = start_ind
    area_ind = []
    cr = Control_params()
    while (distance <= max_dist):
        if (ind >= len(spline)):
            break
        p_begin = spline_point(spline[ind], 0.0)
        p_end = spline_point(spline[ind], 1.0)
        if (spline_content_point(y_obsctle, spline[ind])):
            s_star = spline_param_of_point_projection(y_obsctle, spline[ind])
            p_star = spline_point(spline[ind], s_star);
            distance += np.linalg.norm(p_star-p_begin)
            cr.fill_ok(s_star,  spline[ind], ind, y_obsctle, distance)
            return cr
        ind += 1
        distance += np.linalg.norm(p_end-p_begin)
        
    cr.fill_not_ok()
    return cr

def clear_extra_obstcl_poit(sorted_cr_list, border_radius):
    for ind in range(len(sorted_cr_list)-1):
        if ((abs(sorted_cr_list[ind+1].delta)-abs(sorted_cr_list[ind].delta)) > 2*border_radius):
            new_sorted_cr_list = sorted_cr_list[:ind+1]
            return new_sorted_cr_list
    return sorted_cr_list

def print_cr_array(cr_arr):
    if (cr_arr == []):
        print("empty")
    else:
        for cr in cr_arr:
            cr.print_all()
        
def find_cr_for_optimal_obstcle(start_ind, max_dist, spline, obstacles, border_radius):

    cr_arr_pos = []
    cr_arr_neg = []
    for obtcl in obstacles:
        cr = find_control_params(start_ind, max_dist, spline, obtcl)
        if(cr.ok):
            #cr.print_all()
            if(cr.delta >= 0):
                cr_arr_pos.append(cr)
            else:
                cr_arr_neg.append(cr)

    if((cr_arr_pos == [])and(cr_arr_neg==[])):
        return -1
    
    cr_arr_neg.sort() 
    cr_arr_pos.sort() 

    print("negative")
    print_cr_array(cr_arr_neg)
    print("positive")
    print_cr_array(cr_arr_pos)
    
    if(cr_arr_neg == []):
        if abs(cr_arr_pos[0].delta) > border_radius:
            return -1
        else:
            return cr_arr_pos[0], 1
        
    if(cr_arr_pos == []):
        if abs(cr_arr_neg[0].delta) > border_radius:
            return -1
        else:
            return cr_arr_neg[0], 1
    
    
    if abs(cr_arr_neg[0].delta) + abs(cr_arr_pos[0].delta)  > 2*border_radius:
        return (min(cr_arr_neg[0], cr_arr_pos[0]), 1)
    
    #print("--------------------")
    
    cr_arr_pos = clear_extra_obstcl_poit(cr_arr_pos, border_radius)
    cr_arr_neg = clear_extra_obstcl_poit(cr_arr_neg, border_radius)

    #crpos = max(cr_arr_pos)
    #crneg = max(cr_arr_neg)
    #crpos.print_all()
    #crneg.print_all()
    
    #cropt = min(crpos,crneg)
    #print("optimal")
    #cropt.print_all()
    print("Тут")
    return (min(max(cr_arr_pos), max(cr_arr_neg)), -1)
   
def dubins_path_is_possible(point1, dir1, point2, dir2, max_turning_radius):
    theta  = np.pi/2
    rot = np.array([[cos(theta), -sin(theta), 0 ], [sin(theta), cos(theta), 0],  [0, 0, 1]])
    dir1_ortgn = np.dot(rot, dir1)
    dir2_ortgn = np.dot(rot, dir2)

    dir1_ortgn = dir1_ortgn/np.linalg.norm(dir1_ortgn)
    dir2_ortgn = dir2_ortgn/np.linalg.norm(dir2_ortgn)
    x1_c_p = point1 + dir1_ortgn*max_turning_radius
    x1_c_n = point1 - dir1_ortgn*max_turning_radius
    x2_c_p = point2 + dir2_ortgn*max_turning_radius
    x2_c_n = point2 - dir2_ortgn*max_turning_radius
    #fig3 = plt.figure(figsize = (15,8))
    #ax = plt.axes()
    #ax = draw_obstacles([x1_c_p, x1_c_n, x2_c_p, x2_c_n], ax, max_turning_radius)
    if((np.linalg.norm(x1_c_p-x2_c_n) <= 2*max_turning_radius) or(np.linalg.norm(x2_c_p-x1_c_n) <= 2*max_turning_radius)):
        return False
    return True
    
    
def find_nearest_possible_spline_index_to_turn(point_middle, dir_middle, spline, start_index, max_distance, max_turning_radius, forward):
    distance = 0

    area_ind = []
    ind = start_index
    if forward:
        bias = 1
    else:
        bias = -1
        
    while (distance <= 20):
        #print("next", ind, max_turning_radius)
        if ((ind > len(spline)-1) or (ind < 0)):
            break
        p_new = spline_point(spline[ind], 0.0)
        p_end = spline_point(spline[ind], 1.0)
        dir_new = spline_point_derv(spline[ind], 0.0)
        if(dubins_path_is_possible(point_middle, dir_middle, p_new, dir_new, max_turning_radius)==True):
            return ind
        
        ind += bias
        distance += np.linalg.norm(p_end-p_new)
    return -1
    
    
    
def estimate_q_maneure(start_ind, max_dist, spline, obstacles, border_radius, max_turning_radius):
    
    res_find = find_cr_for_optimal_obstcle(start_ind, max_dist, spline, obstacles, border_radius)
    if(res_find == -1):
        return -1
    obscl_cr, sign = res_find
    #obscl_cr.print_all()
    obst_border_point = obscl_cr.pos_obsct - sign*obscl_cr.Delta*border_radius/abs(obscl_cr.delta)
    obst_border_derv_point = spline_point_derv(spline[obscl_cr.cfs_index],obscl_cr.sstar )
    
    
    
    obst_border_fi = np.arctan2(obst_border_derv_point[1],obst_border_derv_point[0])
    q_obstcl = (obst_border_point[0], obst_border_point[1], obst_border_fi)
    ind_spline_begin = find_nearest_possible_spline_index_to_turn(obst_border_point, obst_border_derv_point, spline, obscl_cr.cfs_index, max_dist, max_turning_radius, False)
    ind_spline_end = find_nearest_possible_spline_index_to_turn(obst_border_point, obst_border_derv_point, spline, obscl_cr.cfs_index, max_dist, max_turning_radius, True)
    if((ind_spline_begin==-1 or(ind_spline_end==-1))):
        return -2

    q0 = spline_find_point_state2d(spline[ind_spline_begin], 0.0)
    q1 = spline_find_point_state2d(spline[ind_spline_end],0)
    return (q0, q_obstcl, q1), (ind_spline_begin, ind_spline_end)

def rotate(A,B,C):
    return (B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0])

def jarvismarch(A):
    n = len(A)
    P = [ i for i in range(n)]
    # start point
    for i in range(1,n):
        if A[P[i]][0]<A[P[0]][0]: 
            P[i], P[0] = P[0], P[i]  
    H = [P[0]]
    del P[0]
    P.append(H[0])
    while True:
        right = 0
        for i in range(1,len(P)):
            if rotate(A[H[-1]],A[P[right]],A[P[i]])<0:
                right = i
        if P[right]==H[0]: 
            break
        else:
            H.append(P[right])
            del P[right]
    return H     
