import reeds_shepp
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import dubins
from spline_math import *
from obstacle_maneuvre import *
import pandas as pd
import sys
import os

sys.path.append(os.getcwd() + "/ReedsSheppPath")
sys.path.append(os.getcwd() + "/HybridAStar")

try:
    from dynamic_programming_heuristic import calc_distance_heuristic
    import reeds_shepp_path_planning as rs
    from car import *
    from hybrid_a_star import *
except Exception:
    raise

    

def plot_2d(x_line, y_line):
    fig = plt.figure(figsize = (20,20))
    ax = plt.axes()
    ax.plot(x_line, y_line, 'gray')
    ax.scatter(x_line, y_line, cmap='Greens')
    
def plot_3d(x_line, y_line, z_line):
    fig = plt.figure(figsize = (20,20))
    ax = plt.axes(projection='3d')
    ax.plot3D(x_line, y_line, z_line, 'gray')
    ax.scatter3D(x_line, y_line, z_line, cmap='Greens')
    
def draw_obstacles(ax, obstcl_arr, min_obstcle_avoid_radius, color = 'k'):
    for obstc in obstcl_arr:
        ax.scatter(obstc[0], obstc[1], c = "k")
        circle1 = plt.Circle((obstc[0], obstc[1]), min_obstcle_avoid_radius, color=color, fill=False)
        ax.add_patch(circle1)


def make_points_from_relative_data(obsracl_realt_list, spline_cfg):
    theta  = np.pi/2
    rot = np.array([[0, -sin(theta), 0 ], [sin(theta), cos(theta), 0],  [0, 0, 1]])
    x0 = spline_point(spline_cfg, 0)
    mov_dir = spline_point_derv(spline_cfg,0)
    dir1_ortgn = np.dot(rot, mov_dir)
    print("dir_ort ",dir1_ortgn)
    mov_dir = mov_dir/np.linalg.norm(mov_dir)
    dir1_ortgn = dir1_ortgn/np.linalg.norm(dir1_ortgn)
    
    
    obstacles_abs = []
    for point in obsracl_realt_list:
        x_new = x0 + point[1]*dir1_ortgn + point[0]*mov_dir
        obstacles_abs.append((x_new[0],x_new[1], 0))
        
    return  obstacles_abs


def make_heuvristic_potential(goal, x, y, spline):
    potential = np.zeros((len(x),len(y))) 
    x_c = goal[0]
    y_c = goal[1]
    for ii in range(len(x)):
        for jj in range(len(y)):
            X = x[ii]
            Y = y[jj]
            cr = find_control_params(1,60,spline,(X,Y,0))
            if(cr.ok == True):
                traj_dist = abs(cr.delta)
                #print("ok")
            else:
                traj_dist = 0
            dist_evkl = np.sqrt((X-x_c)**2 +(Y-y_c)**2)
            potential[jj][ii] = 0.2*dist_evkl  + 1.0*traj_dist           
    return potential

def make_curve_elem(path):
    c_types = []
    yaw = path.yaw_list
    for ii in range(len(yaw)-1):
        if(path.direction_list[ii+1] == True):
            d = 1
        else:
            d = -1   

        if(abs(yaw[ii+1] - yaw[ii])<0.000001):
            c_types.append((d, "S"))        
        elif( yaw[ii+1]*yaw[ii] < 0):      
            c_types.append(c_types[-1])       
        elif((yaw[ii+1]) > (yaw[ii])):     
            if(d==1):
                mark = "L"
            else:
                mark = "R"                  
            c_types.append((d, mark))           
        elif((yaw[ii+1]) < (yaw[ii])):
            if(d==1):
                mark = "R"
            else:
                mark = "L"               
            c_types.append((d, mark) )

    lenth_cur = 0
    curve_types = []
    points_ind = [0]
    
    for ii in range(len(c_types)-1):
        if(c_types[ii] == c_types[ii+1]):
            lenth_cur += 0.1*c_types[ii][0]
        else:
            lenth_cur += 0.1*c_types[ii][0]
            curve_types.append((round(lenth_cur,2), c_types[ii][1]))
            lenth_cur = 0
            points_ind.append(ii+1)
    
    last_ind = len(c_types) - 1
    if(c_types[last_ind] == c_types[last_ind-1]):                  
        curve_types.append((round(0.1*c_types[last_ind][0]+lenth_cur, 2), c_types[last_ind][1]))         
    else:
        curve_types.append((round(0.1*c_types[last_ind][0], 2), c_types[last_ind][1]))  
        
    points_ind.append(last_ind+1)
    
    return curve_types, c_types, points_ind

def make_obstacle_tree(obstacles_full, visible_radius, start):
    ox, oy = [], []
    for obstcl in obstacles_full:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    indexes = obstacle_kd_tree.query_ball_point(start[:2],visible_radius)
    obstacles = [obstacles_full[ind] for ind in indexes]
    ox, oy = [], []
    for obstcl in obstacles:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    return obstacles, obstacle_kd_tree

def main():
    df = pd.read_csv (os.getcwd()+"/surf_waypoints/car3.csv") 

    x_line = df["x"].to_numpy() 
    y_line = df["y"].to_numpy() 
    z_line = df["z"].to_numpy()

    set_initial = np.hstack((x_line[:, np.newaxis], y_line[:, np.newaxis], 0*z_line[:, np.newaxis]))
    spline = M_spline_from_set(set_initial)
    
    obstacles_full = make_points_from_relative_data([       (0.01, 0.5, 0),
                                                            (3.1, -0.8),
                                                            (1.0,-1.9),
                                                            (1.0, -2.6),
                                                            (-1.0, -5.0),
                                                            (-2,-4),
                                                            (0, -4.7),
                                                            (2,-4),
                                                            (4,-2),
                                                            (0,-0.3),
                                                            (-1.6,1.5),
                                                            (-3,2),
                                                            (-5,-4),
                                                            (1.7,-4.3),
                                                            ( -4, -4),
                                                            (-4.4,2),
                                                            (2.2,-1.2),
                                                            (1.8,-0.5)
                                                            ], 
                                                            spline[23])[:]
 
    min_obstcle_avoid_radius = 1.0
    visible_radius = 100.5

    # Set Initial parameters
    start = [-17.5, 13.0, np.deg2rad(140.0)]

    border_size = 30
    border = [ [start[0] - border_size , start[0] + border_size], [ start[1] - border_size, start[1] + border_size ] ]
    obstacles, obstacle_kd_tree =  make_obstacle_tree(obstacles_full, visible_radius, start)
    start_ind = 1
    max_dist = 50
    cr_arr = []

    for obtcl in obstacles:
        cr = find_control_params(start_ind, max_dist, spline, obtcl)
        if((cr.ok) and abs(cr.delta)<=1):
            #cr.print_all()
            cr_arr.append(cr)

    cr_arr.sort()
    goal_spline_ind = 20 ##??
    if(cr_arr !=[]):
        goal_spline_ind = cr_arr[-1].cfs_index
  
    search_ares = [list(spline_find_point_state2d(spline[goal_spline_ind + ind], 0)) for ind in range(3,10)] 
    print(f"goal index is {goal_spline_ind}   {search_ares[-1]}")

    print("start : ", start)
    
    show_animation = False
    fig = plt.figure(figsize = (10,10))
    ax = plt.axes()
    plt.grid(True)
    #plt.axis("equal")
    plt.xlabel("x, m")
    plt.ylabel("y, m")
    plt.gca().set_aspect('equal', adjustable='box')
  
    draw_obstacles(ax, obstacles, min_obstcle_avoid_radius)
    ax.plot(x_line, y_line, 'gray')


    goal = search_ares[-1]
    rs.plot_arrow(start[0], start[1], start[2])
    rs.plot_arrow(goal[0], goal[1], goal[2])
    plot_car(start[0], start[1], start[2])
    #plt.xlim(-27, -13)
    #plt.ylim(7, 20)
    path = hybrid_a_star_planning(
        start, search_ares, border, obstacle_kd_tree, min_obstcle_avoid_radius, _show_animation_ = show_animation)

    x = np.array(path.x_list[:])
    y = np.array(path.y_list[:])
    yaw = np.array(path.yaw_list[:])
    

    print(" plot!!")
    plot_car(start[0], start[1], start[2])
    #rs.plot_arrow(start[0], start[1], start[2])
    plt.plot(x, y, "-r", label="Hybrid A* path")
    plot_car(x[-1], y[-1], yaw[-1])

    plt.grid(True)
    plt.gca().set_aspect('equal', adjustable='box')
    print(" done!!")
    bact_to_route_elements, _ , elem_p_ind = make_curve_elem(path)
    print(f"back to route elements = {bact_to_route_elements}")
#    
    #print(path_rs_final.lengths)
    #print(path_rs_final.ctypes)
    #print(path_rs_final.x[0], path_rs_final.y[0], path_rs_final.yaw[0])
    #print(path_rs_final.x[-1], path_rs_final.y[-1], path_rs_final.yaw[-1])

    
    f = open("Hybride_a_srat_c++/result_c++.txt")
    data = [ list(map(float, xyz.split())) for xyz in f.readlines()  ]
    data = np.array(data)
    #plt.scatter(data[:,0], data[:,1], c = 'g',label="Hybrid A* path c++")
    plt.plot(data[:,0], data[:,1], c = 'g',label="Hybrid A* path c++")
    
    #print(data)
    f = open("Hybride_a_srat_c++/points.txt")
    data = [ list(map(float, xyz.split())) for xyz in f.readlines()  ]
    data = np.array(data)
    #plt.plot(data[:,0], data[:,1], "-k", label="Hybrid A* path c++")
    plt.scatter(data[:,0], data[:,1], c="b", s = 50)



#    plt.scatter(path_rs_final.x[0], path_rs_final.y[0], c='r',s = 100)
#    plt.scatter(x[elem_p_ind], y[elem_p_ind], c='b',s = 50)
#    plot_car(start[0], start[1], start[2])
#    rs.plot_arrow(start[0], start[1], start[2])
#    plt.title(f"Back to route elements = {bact_to_route_elements}")
#    plt.gca().set_aspect('equal', adjustable='box')
#    #plt.draw()

    plt.show()
    if(0):
        fig = plt.figure()
        x = np.linspace(x_line.min(), x_line.max()-6, 100)
        y = np.linspace(y_line.min(), y_line.max(), 100)
        #draw_obstacles(ax, obstacles_full[:6], min_obstcle_avoid_radius)
        plt.plot(x_line, y_line, 'gray')
        plt.scatter(x_line, y_line, cmap='Greens')
        print("start!!")
        Z_heuvr = make_heuvristic_potential(search_ares[-1], x, y, spline)
        print("make!!")
        cs = plt.contourf(x, y, Z_heuvr, levels=np.arange(0, Z_heuvr.max(), 0.8),  cmap="hot") 
        cbar = fig.colorbar(cs, orientation='vertical')
        plt.xlim(x.min(), x.max())
        plt.ylim(y.min(), y.max())
        plot_car(-14, 14, 40)
        plt.xlabel("x, m")
        plt.ylabel("y, m")
        plt.gca().set_aspect('equal', adjustable='box')
      
        #plt.draw()
        plt.show()

    print("All done!!")
if __name__ == '__main__':
    main()
