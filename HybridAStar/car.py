"""

Car model for Hybrid A* path planning

author: Zheng Zh (@Zhengzh)

"""
from skimage.transform import *
from math import sqrt, cos, sin, tan, pi
from matplotlib.image import BboxImage
from matplotlib.transforms import Bbox, TransformedBbox
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation as Rot
mover = plt.imread('lawn_mover.jpg')

WB = 0.75  # rear to front wheel
W = 0.5  # width of car
LF = 0.75  # distance from rear to vehicle front end
LB = 0.0  # distance from rear to vehicle back end
R = 0.33

W_BUBBLE_DIST = (LF - LB) / 2.0


# vehicle rectangle vertices
VRX = [LF, LF, -LB, -LB, LF]
VRY = [W / 2, -W / 2, -W / 2, W / 2, W / 2]


#def check_car_collision(x_list, y_list, yaw_list, config, kd_tree, avoid_r = 1.0):
#    for i_x, i_y, i_yaw in zip(x_list, y_list, yaw_list):
#        #cx = i_x + W_BUBBLE_DIST * cos(i_yaw)
#        #cy = i_y + W_BUBBLE_DIST * sin(i_yaw)
#        
#        ids = kd_tree.query_ball_point([i_x, i_y], avoid_r+)
#        if ((i_x > config.max_x_m) or (i_x < config.min_x_m) or (i_y > config.max_y_m) or (i_y < config.min_x_m)):
#            return False
#
#        if not ids:
#            continue
#        else:
#            return False
#
#        #if not rectangle_check(i_x, i_y, i_yaw,
#                               #[ox[i] for i in ids], [oy[i] for i in ids]):
#            #return False  # collision
#
#    return True  # no collision

def check_car_collision(x_, y_, yaw_, kd_tree, avoid_r = 1.0):
    for (x, y, yaw) in zip(x_, y_, yaw_):
        x_s = x + cos(yaw)*WB/4
        y_s = y + sin(yaw)*WB/4
        ids = kd_tree.query_ball_point([x_s, y_s], avoid_r+R)
        if ids:
            return False
        x_s = x + cos(yaw)*(2*WB/4)
        y_s = y + sin(yaw)*(2*WB/4)
        ids = kd_tree.query_ball_point([x_s, y_s], avoid_r+R)
        if ids:
            return False
        x_s = x + cos(yaw)*(3*WB/4)
        y_s = y + sin(yaw)*(3*WB/4)
        ids = kd_tree.query_ball_point([x_s, y_s], avoid_r+R)
        if ids:
            return False
    return True  # no collision

def rectangle_check(x, y, yaw, ox, oy):
    # transform obstacles to base link frame
    rot = Rot.from_euler('z', yaw).as_matrix()[0:2, 0:2]
    for iox, ioy in zip(ox, oy):
        tx = iox - x
        ty = ioy - y
        converted_xy = np.stack([tx, ty]).T @ rot
        rx, ry = converted_xy[0], converted_xy[1]

        if not (rx > LF or rx < -LB or ry > W / 2.0 or ry < -W / 2.0):
            return False  # no collision

    return True  # collision


def plot_arrow(x, y, yaw, length=0.6, width=0.5, fc="r", ec="k"):
    """Plot arrow."""
    if not isinstance(x, float):
        for (i_x, i_y, i_yaw) in zip(x, y, yaw):
            plot_arrow(i_x, i_y, i_yaw)
    else:
        plt.arrow(x, y, length * cos(yaw), length * sin(yaw),
                  fc=fc, ec=ec, head_width=width, head_length=width, alpha=0.4)




# https://stackoverflow.com/questions/25329583/matplotlib-using-image-for-points-on-plot
def plot_image_car(x, y, im, ax, l = 2):
        bb = Bbox.from_bounds(x, y, l, l)
        bb2 = TransformedBbox(bb, ax.transData)
        bbox_image = BboxImage(bb2,
                               norm=None,
                               origin=None,
                               clip_on=False)
        bbox_image.set_data(im)
        ax.add_artist(bbox_image)



from scipy import ndimage

def plot_car(x, y, yaw, l = 2):
    car_color = '-k'
    c, s = cos(yaw), sin(yaw)
    rot = Rot.from_euler('z', -yaw).as_matrix()[0:2, 0:2]
    car_outline_x, car_outline_y = [], []
    for rx, ry in zip(VRX, VRY):
        converted_xy = np.stack([rx, ry]).T @ rot
        car_outline_x.append(converted_xy[0]+x)
        car_outline_y.append(converted_xy[1]+y)

    arrow_x, arrow_y, arrow_yaw = c * 0.1 + x, s * 0.1 + y, yaw
    ax = plt.gca()
    plot_arrow(arrow_x, arrow_y, arrow_yaw)

    plt.plot(car_outline_x, car_outline_y, car_color)

def plot_car_image(x, y, yaw, l = 2):
    car_color = '-k'
    c, s = cos(yaw), sin(yaw)
    # rot = Rot.from_euler('z', -yaw).as_matrix()[0:2, 0:2]
    # car_outline_x, car_outline_y = [], []
    # for rx, ry in zip(VRX, VRY):
    #     converted_xy = np.stack([rx, ry]).T @ rot
    #     car_outline_x.append(converted_xy[0]+x)
    #     car_outline_y.append(converted_xy[1]+y)

    arrow_x, arrow_y, arrow_yaw = c * 0.5 + x, s * 0.5 + y, yaw
    ax = plt.gca()
    #plot_arrow(arrow_x, arrow_y, arrow_yaw)


    # plt.plot(car_outline_x, car_outline_y, car_color)

    rotated_mover = ndimage.rotate(mover, angle=np.rad2deg(yaw+np.pi), mode='nearest')
    plot_image_car(x-1.0, y-1.0, rotated_mover, ax, l = l)


def pi_2_pi(angle):
    return (angle + pi) % (2 * pi) - pi


def move(x, y, yaw, distance, u, L=WB):
    x += distance * cos(yaw)
    y += distance * sin(yaw)
    #yaw += pi_2_pi(distance * tan(steer) / L)  # distance/2
    yaw = pi_2_pi(yaw + distance * u)
    return x, y, yaw


def main():
    x, y, yaw = 0., 0., 1.
    plt.axis('equal')
    plot_car(x, y, yaw)
    plt.show()


if __name__ == '__main__':
    main()
