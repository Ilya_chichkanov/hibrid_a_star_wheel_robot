"""

Hybrid A* path planning

author: Zheng Zh (@Zhengzh)

"""

import heapq
import math
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import cKDTree

sys.path.append(os.path.dirname(os.path.abspath(__file__))
                + "/../ReedsSheppPath")

sys.path.append(os.path.dirname(os.path.abspath(__file__))
                + "/../")
try:
    from dynamic_programming_heuristic import calc_distance_heuristic
    import reeds_shepp_path_planning as rs
    import rs_elements
    from reeds_shepp_path_planning import reeds_shepp_path_planning
    from car import move, check_car_collision, WB, plot_car
    from obstacle_maneuvre import find_control_params
except Exception:
    raise


XY_GRID_RESOLUTION = 0.4  # [m]
YAW_GRID_RESOLUTION = np.deg2rad(5.0)  # [rad]
MOTION_RESOLUTION = 0.1  # [m] path interpolate resolution

MAX_U = 0.5
N_STEER = 3  # number of steer command
VR = 1.0  # robot radius
SB_COST = 30.0  # switch back penalty cost
BACK_COST = 3.0  # backward penalty cost
STEER_CHANGE_COST = 0.2  # steer angle change penalty cost
STEER_COST = 0.0  # steer angle change penalty cost
STEP_LENGTH = 0.6

show_animation = False
START = True
class Node:

    def __init__(self, x_ind, y_ind, yaw_ind, direction,
                 x_list, y_list, yaw_list, directions,
                 steer=0.0, parent_index=None, cost=None, tr = False):
        self.x_index = x_ind
        self.y_index = y_ind
        self.yaw_index = yaw_ind
        self.direction = direction
        self.x_list = x_list
        self.y_list = y_list
        self.yaw_list = yaw_list
        self.directions = directions
        self.steer = steer
        self.parent_index = parent_index
        self.cost = cost
        self.transition = False


class Path:

    def __init__(self, x_list, y_list, yaw_list, direction_list, cost):
        self.x_list = x_list
        self.y_list = y_list
        self.yaw_list = yaw_list
        self.direction_list = direction_list
        self.cost = cost


class Config:

    def __init__(self, border, xy_resolution, yaw_resolution):
        self.min_x_m = border[0][0]
        self.min_y_m = border[1][0]
        self.max_x_m = border[0][1]
        self.max_y_m = border[1][1]
        

        self.min_x = round(self.min_x_m / xy_resolution)
        self.min_y = round(self.min_y_m / xy_resolution)
        self.max_x = round(self.max_x_m / xy_resolution)
        self.max_y = round(self.max_y_m / xy_resolution)

        self.x_w = round(self.max_x - self.min_x)
        self.y_w = round(self.max_y - self.min_y)

        self.min_yaw = round(- math.pi / yaw_resolution)
        self.max_yaw = round(math.pi / yaw_resolution)
        #self.min_yaw = 0
        #self.max_yaw = round(2*math.pi / yaw_resolution)
        self.yaw_w = round(self.max_yaw - self.min_yaw)
        
    def print_all(self):
        print(f"min x {self.min_x} min y {self.min_y} min yaw {self.min_yaw}")
        print(f"max x {self.max_x} max y {self.max_y} max yaw {self.max_yaw}")
        print(f"x_w {self.x_w} y_w {self.y_w} yaw_w {self.yaw_w}")
        

def calc_motion_inputs(current):


    steer_possible_array = [0]
    if(current.steer == -MAX_U):
        steer_possible_array.append(-MAX_U)
    elif(current.steer == MAX_U):
        steer_possible_array.append(MAX_U)
    elif(current.steer == 0):
        steer_possible_array.append(-MAX_U)
        steer_possible_array.append(MAX_U)
        
    for steer in steer_possible_array:
        for d in [1, -1]:
            yield [steer, d]


def get_neighbors(current, config, kd_tree, avoid_radius):
    for steer, d in calc_motion_inputs(current):
        node = calc_next_node(current, steer, d, config, kd_tree, avoid_radius)
        if node and verify_index(node, config):
            yield node


def calc_next_node(current, steer, direction, config, kd_tree, avoid_radius):
    x, y, yaw = current.x_list[-1], current.y_list[-1], current.yaw_list[-1]

    arc_l = XY_GRID_RESOLUTION * 1.5
    x_list, y_list, yaw_list = [], [], []
 
    for _ in np.arange(0, STEP_LENGTH, MOTION_RESOLUTION):
        x, y, yaw = move(x, y, yaw, MOTION_RESOLUTION * direction, steer)
        x_list.append(x)
        y_list.append(y)
        yaw_list.append(yaw)
    
    
    colision =  check_car_collision(x_list, y_list, yaw_list, kd_tree, avoid_radius)
        

    d = direction == 1

    
    global START
    added_cost = 0.0
    if (d != current.direction and START == False):
        added_cost += SB_COST*STEP_LENGTH*STEP_LENGTH

    if(START == True):
        if(colision==False):
             if(check_car_collision([x_list[-1]], [y_list[-1]], [yaw_list[-1]], kd_tree, avoid_radius)==False):
                return None
            
    else:
        if(colision==False):
            return None
    # steer penalty
    added_cost += STEER_COST * abs(steer)*STEP_LENGTH
     
    if(direction == -1):
         added_cost += BACK_COST*STEP_LENGTH#* STEER_COST * abs(steer)
            
    # steer change penalty
    added_cost += STEER_CHANGE_COST * abs(current.steer - steer)

    cost = current.cost + added_cost + STEP_LENGTH
    x_ind = round(x / XY_GRID_RESOLUTION)
    y_ind = round(y / XY_GRID_RESOLUTION)
    yaw_ind = round(yaw / YAW_GRID_RESOLUTION) 
    if abs(yaw_ind) > abs(config.max_yaw):
        print(f"bad yaw {yaw}")

    node = Node(x_ind, y_ind, yaw_ind, d, x_list,
                y_list, yaw_list, len(x_list)*[d],
                parent_index = calc_index(current, config),
                cost=cost, steer=steer)
    return node


def analytic_expansion(current, goals, c, kd_tree, avoid_radius):
    start_x = current.x_list[-1]
    start_y = current.y_list[-1]
    start_yaw = current.yaw_list[-1]
    best_path, best = None, None
    for goal in goals:
        goal_x = goal.x_list[-1]
        goal_y = goal.y_list[-1]
        goal_yaw = goal.yaw_list[-1]

        max_curvature = MAX_U
        paths = rs.calc_paths(start_x, start_y, start_yaw,
                              goal_x, goal_y, goal_yaw,
                              MAX_U, step_size=MOTION_RESOLUTION)

        if not paths:
            continue

       

        for path in paths:
            if check_car_collision(path.x, path.y, path.yaw, kd_tree, avoid_radius):
                cost = calc_rs_path_cost(path)
                if not best or best > cost:
                    best = cost
                    best_path = path
    
    return best_path


def update_node_with_analytic_expansion(current, goal,
                                        c, kd_tree, avoid_radius):
    path = analytic_expansion(current, goal, c, kd_tree, avoid_radius)

    if path:
        if show_animation:
            plt.plot(path.x, path.y)
        f_x = path.x[1:]
        f_y = path.y[1:]
        f_yaw = path.yaw[1:]

        f_cost = current.cost + calc_rs_path_cost(path)
        f_parent_index = calc_index(current, c)

        fd = []
        for d in path.directions[1:]:
            fd.append(d >= 0)

        f_steer = 0.0
        f_path = Node(current.x_index, current.y_index, current.yaw_index,
                      current.direction, f_x, f_y, f_yaw, fd,
                      cost=f_cost, parent_index=f_parent_index, steer=f_steer)
        return True, f_path, path

    return False, None, None


def calc_rs_path_cost(reed_shepp_path):
    cost = 0.0
    
    for length in reed_shepp_path.lengths:
        if length >= 0:  # forward
            cost += length
        else:  # back
            cost += abs(length) * BACK_COST
    
    # switch back penalty
    for i in range(len(reed_shepp_path.lengths) - 1):
        # switch back
        if reed_shepp_path.lengths[i] * reed_shepp_path.lengths[i + 1] < 0.0:
            cost += SB_COST

    # steer penalty
    for course_type in reed_shepp_path.ctypes:
        if course_type != "S":  # curve
            cost += STEER_COST * abs(MAX_U)

    # ==steer change penalty
    # calc steer profile
    n_ctypes = len(reed_shepp_path.ctypes)
    u_list = [0.0] * n_ctypes
    for i in range(n_ctypes):
        if reed_shepp_path.ctypes[i] == "R":
            u_list[i] = - MAX_U
        elif reed_shepp_path.ctypes[i] == "L":
            u_list[i] = MAX_U

    for i in range(len(reed_shepp_path.ctypes) - 1):
        cost += STEER_CHANGE_COST * abs(u_list[i + 1] - u_list[i])
    
    return cost


def hybrid_a_star_planning(start, goal, border, obstacle_kd_tree, obstcle_avoid_radius = 1, _show_animation_ = False, camera = None, fig = None):
    """
    start: start node
    goal: goal node
    obstacle_kd_tree: obstacle_kd_tree
    xy_resolution: grid resolution [m]
    yaw_resolution: yaw angle resolution [rad]
    """
    global START
    START= True
    avoid_radius = obstcle_avoid_radius
    show_animation = _show_animation_
        
    config = Config(border, XY_GRID_RESOLUTION, YAW_GRID_RESOLUTION)
    config.print_all()
    cost=0
    if(check_car_collision([start[0]],[start[1]], [start[2]], obstacle_kd_tree, avoid_radius) ==False):
        cost = 1e5

    start_node = Node(round(start[0] / XY_GRID_RESOLUTION),
                      round(start[1] / XY_GRID_RESOLUTION),
                      round(start[2] / XY_GRID_RESOLUTION), True,
                      [start[0]], [start[1]], [ rs.pi_2_pi(start[2])], [True], cost=cost)
    goal_nodes = []
    for goal_elem in goal:   
        goal_node = Node(round(goal_elem[0] / XY_GRID_RESOLUTION),
                         round(goal_elem[1] / XY_GRID_RESOLUTION),
                         round(goal_elem[2] / XY_GRID_RESOLUTION), True,
                         [goal_elem[0]], [goal_elem[1]], [rs.pi_2_pi(goal_elem[2])], [True])
        
        goal_nodes.append(goal_node)
        
    openList, closedList = {}, {}
    
    h_dp = {}
    pq = []
    openList[calc_index(start_node, config)] = start_node
    heapq.heappush(pq, (calc_cost(start_node, h_dp, config,  goal_nodes[-1]),
                        calc_index(start_node, config)))
    final_path = None
    n_iter = 0
    
    is_updated = False
    final_path = None
    path_rs_final = None
    while True:
        n_iter+=1
        if not openList:
            print("Error: Cannot find path, No open set")
            return None

        cost, c_id = heapq.heappop(pq)
        #print(c_id)
        if c_id in openList:
            current = openList.pop(c_id)
            closedList[c_id] = current
        else:
            continue

        if show_animation:  # pragma: no cover
            plt.plot(current.x_list[-1], current.y_list[-1], "xc")
            # for stopping simulation with the esc key.
            plt.gcf().canvas.mpl_connect(
                'key_release_event',
                lambda event: [exit(0) if event.key == 'escape' else None])
            if len(closedList.keys()) % 20 == 0:
                plt.pause(0.001)

        pos = (current.x_list[-1], current.y_list[-1], 0)
        dist = np.sqrt((current.x_list[-1] - goal[0][0])**2 +  (current.y_list[-1] - goal[0][1])**2) 

        traj_distance_cost = 0
        if(dist < 3.0):
            is_updated, final_path, path_rs_final = update_node_with_analytic_expansion(current, goal_nodes, config, obstacle_kd_tree, avoid_radius)

        if is_updated:
            path = get_final_path(closedList, final_path)
            print(f"n_iter = {n_iter}, len close = {len(closedList)}, len open = { len(openList)}, cost {path.cost}")
            return path
         

        for neighbor in get_neighbors(current, config, obstacle_kd_tree, avoid_radius):
            neighbor_index = calc_index(neighbor, config)

            if neighbor_index in closedList:
                continue

            in_open_list = neighbor_index in openList.keys()
            
            if(in_open_list == True):      
                better_way_found = openList[neighbor_index].cost > neighbor.cost
            else:
                better_way_found = False
            
            if (in_open_list == False or better_way_found == True):
                new_cost = calc_cost(neighbor, h_dp, config, goal_nodes[-1])
                heapq.heappush(
                    pq, (new_cost,
                         neighbor_index))
                openList[neighbor_index] = neighbor

        if(START == True):
            START = False

        if(n_iter > 15000):
            break

    return None



def calc_cost(n, h_dp, c, goal):
    ind = (n.y_index - c.min_y) * c.x_w + (n.x_index - c.min_x)

    if ind not in h_dp:

        goal_x, goal_y, goal_yaw =  goal.x_list[-1], goal.y_list[-1], goal.yaw_list[-1]
        x_cur, y_cur, yaw_cur   =  n.x_list[-1], n.y_list[-1],  n.yaw_list[-1]
        #dist_cost = np.sqrt((x_cur - goal_x)**2 +  (y_cur - goal_y)**2) 
        
        x, y, yaw, ctypes, lengths = reeds_shepp_path_planning(x_cur, y_cur, yaw_cur,
                                      goal_x, goal_y, goal_yaw,
                                      0.5, step_size=0.1) 
        if(lengths !=None):
            dist =  sum(map(abs, lengths))
        else:
            dist =  np.sqrt((x_cur - goal_x)**2 +  (y_cur - goal_y)**2) 
        h_dp[ind] =  dist

    return n.cost + h_dp[ind]
   


def get_final_path(closed, goal_node):
    reversed_x, reversed_y, reversed_yaw = \
        list(reversed(goal_node.x_list)), list(reversed(goal_node.y_list)), \
        list(reversed(goal_node.yaw_list))
    direction = list(reversed(goal_node.directions))
    nid = goal_node.parent_index
    final_cost = goal_node.cost

    while nid:
        n = closed[nid]
        reversed_x.extend(list(reversed(n.x_list)))
        reversed_y.extend(list(reversed(n.y_list)))
        reversed_yaw.extend(list(reversed(n.yaw_list)))
        direction.extend(list(reversed(n.directions)))

        nid = n.parent_index

    reversed_x = list(reversed(reversed_x))
    reversed_y = list(reversed(reversed_y))
    reversed_yaw = list(reversed(reversed_yaw))
    direction = list(reversed(direction))

    # adjust first direction
    direction[0] = direction[1]

    path = Path(reversed_x, reversed_y, reversed_yaw, direction, final_cost)

    return path

"""

def get_final_path(closed, goal_node, last_ind):


    reversed_x  = []
    reversed_y  = []
    reversed_yaw  = []
    direction = []
    nid = last_ind
    while nid:
        n = closed[nid]
        reversed_x.extend(list(reversed(n.x_list)))
        reversed_y.extend(list(reversed(n.y_list)))
        reversed_yaw.extend(list(reversed(n.yaw_list)))
        direction.extend(list(reversed(n.directions)))

        nid = n.parent_index

    reversed_x = list(reversed(reversed_x))
    reversed_y = list(reversed(reversed_y))
    reversed_yaw = list(reversed(reversed_yaw))
    direction = list(reversed(direction))

    # adjust first direction
    direction[0] = direction[1]

    path = Path(reversed_x, reversed_y, reversed_yaw, direction, 100)

    return path, 
"""
def verify_index(node, c):
    x_ind, y_ind = node.x_index, node.y_index
    if c.min_x <= x_ind <= c.max_x and c.min_y <= y_ind <= c.max_y:
        return True

    return False


def calc_index(node, c):
    ind = (node.yaw_index - c.min_yaw) * c.x_w * c.y_w + \
          (node.y_index - c.min_y) * c.x_w + (node.x_index - c.min_x)
    
    
    if ind <= 0:
        print(f"Error(calc_index):  {ind}, {node.yaw_index}, {c.min_yaw}")

    return ind

