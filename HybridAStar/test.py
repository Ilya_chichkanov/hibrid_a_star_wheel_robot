import reeds_shepp
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import dubins
from spline_math import *
from obstacle_maneuvre import *
import pandas as pd
import sys
import os

sys.path.append(os.getcwd() + "/ReedsSheppPath")
sys.path.append(os.getcwd() + "/HybridAStar")

try:
    from dynamic_programming_heuristic import calc_distance_heuristic
    import reeds_shepp_path_planning as rs
    from car import move, check_car_collision, MAX_STEER, WB, plot_car
    from hybrid_a_star import *
except Exception:
    raise

    

df = pd.read_csv (os.getcwd()+"/surf_waypoints/car3.csv")   #read the csv file (put 'r' before the path string to address any special characters in the path, such as '\'). Don't forget to put the file name at the end of the path + ".csv"
print (df)
x_line = df["x"].to_numpy() 
y_line = df["y"].to_numpy() 
z_line = df["z"].to_numpy() 

y_line+=20
x_line+=50

set_initial = np.hstack((x_line[:, np.newaxis], y_line[:, np.newaxis], 0*z_line[:, np.newaxis]))
spline = M_spline_from_set(set_initial)



min_obstcle_avoid_radius = 1.0
max_turning_radius = 2.0
curr_spline_index = 3
max_serching_distanse = 20


from math import cos, sin
def plot_2d(x_line, y_line):
    fig = plt.figure(figsize = (20,20))
    ax = plt.axes()
    ax.plot(x_line, y_line, 'gray')
    ax.scatter(x_line, y_line, cmap='Greens')
    
def plot_3d(x_line, y_line, z_line):
    fig = plt.figure(figsize = (20,20))
    ax = plt.axes(projection='3d')
    ax.plot3D(x_line, y_line, z_line, 'gray')
    ax.scatter3D(x_line, y_line, z_line, cmap='Greens')
    
def draw_obstacles(ax, obstcl_arr, min_obstcle_avoid_radius, color = 'g'):
    for obstc in obstcl_arr:
        ax.scatter(obstc[0], obstc[1])
        circle1 = plt.Circle((obstc[0], obstc[1]), min_obstcle_avoid_radius, color=color, fill=False)
        ax.add_patch(circle1)


def make_points_from_relative_data(obsracl_realt_list, spline_cfg):
    theta  = np.pi/2
    rot = np.array([[0, -sin(theta), 0 ], [sin(theta), cos(theta), 0],  [0, 0, 1]])
    x0 = spline_point(spline_cfg, 0)
    mov_dir = spline_point_derv(spline_cfg,0)
    dir1_ortgn = np.dot(rot, mov_dir)
    
    mov_dir = mov_dir/np.linalg.norm(mov_dir)
    dir1_ortgn = dir1_ortgn/np.linalg.norm(dir1_ortgn)
    
    
    obstacles_abs = []
    #print(np.linalg.norm(dir1_ortgn))
    #print(np.linalg.norm(mov_dir))
    #print(dir1_ortgn)
    #print(mov_dir)
    for point in obsracl_realt_list:
        #print(point)
        x_new = x0 + point[1]*dir1_ortgn + point[0]*mov_dir
        obstacles_abs.append((x_new[0],x_new[1], 0))
        
    return  obstacles_abs

def main():
    obstacles = make_points_from_relative_data([(0.01, 0.5, 0),
                                                #(3.1, -0.8),
                                                (1.0,-1.9),
                                                 (1.0, -2.6),
                                                 (-1.0, -5.0),
                                                 (-2,-4),
                                                 (0, -4.7),
                                                  (2,-4),
                                                (4,-2),
                                               (0,-0.3),
                                              (-1.6,1.5),
                                               (-3,2),
                                               (-5,-4),
                                               #(5,-6.5)
                                               ( -4, -4),
                                                (-4.4,2)
                                               ], spline[10])

    obstacles = obstacles[:]
    start_ind = 3
    max_dist = 20
    cr_arr = []

    for obtcl in obstacles:
        cr = find_control_params(start_ind, max_dist, spline, obtcl)
        if((cr.ok) and abs(cr.delta)<=1):
            #cr.print_all()
            cr_arr.append(cr)

    cr_arr.sort()
    #cr_arr[-1].print_all()

    search_ares = [list(spline_find_point_state2d(spline[cr_arr[-1].cfs_index + ind], 0)) for ind in range(3,10)] 

    ox, oy = [], []

    # Set Initial parameters
    start = [34.0, 34.5, np.deg2rad(70.0)]

    goal = list(spline_find_point_state2d(spline[18],0))

    print("start : ", start)
    print("goal : ", goal)

    fig = plt.figure()
    ax = plt.axes()

    draw_obstacles(ax, obstacles, min_obstcle_avoid_radius)
    ax.plot(x_line, y_line, 'gray')
    ax.scatter(x_line, y_line, cmap='Greens')
    for obstcl in obstacles:
        ox.append(obstcl[0])
        oy.append(obstcl[1])

    ox, oy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((ox, oy)).T)


    if show_animation:
        plt.plot(ox, oy, ".k")
        rs.plot_arrow(start[0], start[1], start[2], fc='g')
        rs.plot_arrow(goal[0], goal[1], goal[2])

        plt.grid(True)
        plt.axis("equal")

    (path, path_rs_final) = hybrid_a_star_planning(
        start, search_ares, obstacle_kd_tree, XY_GRID_RESOLUTION, YAW_GRID_RESOLUTION)

    path = path[0]
    x = path.x_list
    y = path.y_list
    yaw = path.yaw_list

    ax = plt.axes()
    draw_obstacles(ax, obstacles, min_obstcle_avoid_radius)
    ax.plot(x_line, y_line, 'gray')
    ax.scatter(x_line, y_line, cmap='Greens')

    if show_animation:

        for ii, (i_x, i_y, i_yaw) in enumerate(zip(x, y, yaw)):
            if(ii==0):
                plot_car(i_x, i_y, i_yaw)
            #plt.cla()
            plt.plot(ox, oy, ".k")
            plt.plot(x, y, "-r", label="Hybrid A* path")
            plt.grid(True)
            plt.axis("equal")
            #
            plt.pause(0.0001)
        plot_car(i_x, i_y, i_yaw)

    print(" done!!")
    print(path_rs_final.lengths)
    print(path_rs_final.ctypes)
    print(path_rs_final.x[0], path_rs_final.y[0], path_rs_final.yaw[0])
    print(path_rs_final.x[-1], path_rs_final.y[-1], path_rs_final.yaw[-1])
    plt.scatter(path_rs_final.x[0], path_rs_final.y[0], c='r',s = 100)
    
if __name__ == '__main__':
    main()
