#pragma once
#include <Eigen/Dense>

typedef Eigen::Vector2f Vector2;
typedef Eigen::Vector3f Vector3;
typedef Eigen::Vector3d Vector3d;
typedef Eigen::Vector4f Vector4;
using namespace Eigen;
