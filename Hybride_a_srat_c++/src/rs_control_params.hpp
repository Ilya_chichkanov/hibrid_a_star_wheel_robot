/*
 * rs_control_params.hpp
 *
 *  Created on: 21 янв. 2022 г.
 *      Author: ilia
 */

#ifndef RS_CONTROL_PARAMS_HPP_
#define RS_CONTROL_PARAMS_HPP_
#include "rs_elements_math.h"

bool check_point_belong_rs_element(const ReedsShepp_element& rs_element,  const Vector2f& point, float u_max);
void find_cr_param_rs_element(const ReedsShepp_element& rs_element, const Vector2f& point, float u_max, ControlParams_rs& cr);
std::ostream& operator << (std::ostream &os, const ControlParams_rs &contr_param);
void test_rs_elements();



#endif /* RS_CONTROL_PARAMS_HPP_ */
