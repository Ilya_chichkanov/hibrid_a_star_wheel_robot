#pragma once
#include <vector>
#include "definitions.hpp"

struct ControlParams
{
    bool ok;
    
    Matrix<float, 3, 4> cfs;
    int cfs_index;
    float sstar;
    Vector3f p;
    Vector3f dp;
    Vector3f ddp;
    
    Vector3f Delta;
    float delta;
    
};

std::ostream& operator << (std::ostream &os, const ControlParams &contr_param);

void extend_waypoints_set(std::vector<Vector3f>& set);

void fill_control_params_not_ok(ControlParams& cr);

void fill_control_params_ok(ControlParams& cr, double s, const Matrix<float, 3, 4>& cfs, int index, const Vector3f& y);

std::vector<Matrix<float, 3, 4>> M_spline_from_set(std::vector<Vector3f> set);;

Vector3f spline_point(const Matrix<float,3, 4>& cfs, float s);

Vector3f spline_2d_state(const Matrix<float,3, 4>& cfs, float s);

Vector3f spline_point_derv(const Matrix<float, 3, 4>& cfs, double s);

Vector3f spline_point_double_derv(const Matrix<float, 3, 4>& cfs, double s);

float spline_projection_criteria(const Matrix<float, 3, 4>& cfs, float s, const Vector3f& y);

float spline_projection_criteria_derv(const Matrix<float, 3, 4>& cfs, double s, const Vector3f& y);

float spline_param_of_point_projection_all_spline(const Vector3f& y, const Matrix<float, 3, 4>& cfs);

bool spline_content_point(const Vector3f& y, const Matrix<float, 3, 4>& spline);

float spline_param_of_point_projection_first_order(const Vector3f& y, const Matrix<float, 3, 4> & spline);

float spline_param_of_point_projection(const Vector3f& y, const Matrix<float, 3, 4>& cfs);

void find_control_params(const Vector3f& y, const std::vector<Matrix<float, 3, 4>>& spline, size_t start_int, float search_distance, ControlParams& cr);
