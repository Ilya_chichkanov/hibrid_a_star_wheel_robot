/*
 * rs_elements.h
 *
 *  Created on: 14 дек. 2021 г.
 *      Author: ilia
 */

#ifndef RS_ELEMENTS_MATH_H_
#define RS_ELEMENTS_MATH_H_
#include "definitions.hpp"
#include <vector>

enum SegmentType { STRAIGHT=0, LEFT=1, RIGHT=2 };

struct ReedsShepp_element
{
	SegmentType  type;
	float length;
	Vector3 start_point;
};

struct Path_RS
{
	Path_RS():total_length(0){};
	float total_length;
	std::vector<Vector3> pose_list;
	std::vector<ReedsShepp_element> element_list;
	void clear()
	{
		total_length = 0;
		pose_list.clear();
		element_list.clear();
	}

};

struct ControlParams_rs
{
	ControlParams_rs():
		ok(false),
	    cfs_index(0),
	    sstar(0),
	    delta(0)
    {
    	p = dp = ddp = Delta = {0, 0, 0};
    }
	bool ok;
	int cfs_index;
	float sstar;
	Vector3f p;
	Vector3f dp;
	Vector3f ddp;
	Vector3f Delta;
	float delta;
};

void rs_p_dp(const ReedsShepp_element& rs_element, float s_star, float u_max, Vector2f& p, Vector2f& dp);
Path_RS rs_make_path(const Vector3f& start, const Vector3f& goal,  float u_max, float step_size);
void    rs_path_seed(float resolution, float u_max, Path_RS& path);
Vector3 rs_expand(const ReedsShepp_element& rs_elem, float u_max, float s);


#endif /* RS_ELEMENTS_MATH_H_ */
