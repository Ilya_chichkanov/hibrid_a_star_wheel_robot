#include <iostream>
#include "quat_math.hpp"
#include "spline_math.hpp"

using namespace std;

void fill_control_params_ok(ControlParams& cr, double s, const Matrix<float, 3, 4>& cfs, int index, const Vector3f& y)
{
        cr.ok = true;
        cr.cfs = cfs;
        cr.cfs_index = index;
        cr.sstar = s;
        cr.p = spline_point(cfs, s);
        cr.dp = spline_point_derv(cfs, s);
        cr.ddp = spline_point_double_derv(cfs, s);
        cr.Delta = y - cr.p;
        cr.delta = cr.Delta.norm();
}

void fill_control_params_not_ok(ControlParams& cr)
{
        cr.ok = false;
        cr.cfs = Matrix<float, 3, 4>::Zero();
        cr.cfs_index = -1;
        cr.sstar = -1;
        cr.p = Vector3f::Zero();
        cr.dp = Vector3f::Zero();
        cr.ddp = Vector3f::Zero();
        cr.Delta = Vector3f::Zero();
        cr.delta = -1;
}

void extend_waypoints_set(std::vector<Vector3f>& set)
{
    int set_length = set.size();
    
    // begin
    Vector3f d_begin = set.at(1) - set.at(0);
    Vector3f set_begin = set.at(0) - d_begin;
    
    // end
    Vector3f d_end = set.at(set_length-1) - set.at(set_length-2);
    Vector3f set_end_1 = set.at(set_length-1) + d_end;
    Vector3f set_end_2 = set.at(set_length-1) + 2*d_end;
    
    // extend
    set.insert(set.begin(), set_begin);
    set.push_back(set_end_1);
    set.push_back(set_end_2);

}

std::ostream& operator << (std::ostream &os, const ControlParams &contr_param)
{
	os<<"ok "<<contr_param.ok<<std::endl;
	os<<"index "<<contr_param.cfs_index<<std::endl;
	os<<"sstar "<<contr_param.sstar<<std::endl;
	os<<"p "<<contr_param.p<<std::endl;
	os<<"delta "<<contr_param.delta<<std::endl;
	return os;
}

std::vector<Matrix<float, 3, 4>> M_spline_from_set(std::vector<Vector3f> set)
{
    int set_length = set.size();
    extend_waypoints_set(set);

    Matrix<float, 4, 4> M;
    M << Matrix<float, 4, 1>(1, -3, 3, -1), Matrix<float, 4, 1>(4, 0, -6, 3),
    Matrix<float, 4, 1>(1, 3, 3, -3), Matrix<float, 4, 1>(0, 0, 0, 1);
    M = M / 6;
    Matrix<float, 4, 4> Mt = M.transpose();
    
    Matrix<float, 4, 4> A;
    A << Matrix<float, 4, 1>(1, 0, 0, 0), Matrix<float, 4, 1>(0, 1, 0, 0),
    Matrix<float, 4, 1>(0, 0, 1, 0), Matrix<float, 4, 1>(0, 0, 0, 1);
               
    std::vector<Matrix<float, 3, 4>> spline;
    for (int i = 1; i < set_length; ++i)
    {
        Matrix<float, 3, 4> ri;
        ri << set.at(i-1), set.at(i), set.at(i+1), set.at(i+2);
        Matrix<float, 3, 4> si = ri * Mt * A;
        spline.push_back(si);
    }
    
    return spline;
}

Vector3f spline_point(const Matrix<float,3, 4>& cfs, float s)
{
    float x = cfs(0, 3) * s*s*s + cfs(0, 2) * s*s + cfs(0, 1) * s + cfs(0, 0);
    float y = cfs(1, 3) * s*s*s + cfs(1, 2) * s*s + cfs(1, 1) * s + cfs(1, 0);
    float z = cfs(2, 3) * s*s*s + cfs(2, 2) * s*s + cfs(2, 1) * s + cfs(2, 0);    
    return Vector3f(x, y, z);
}

Vector3f spline_2d_state(const Matrix<float,3, 4>& cfs, float s)
{
    float x = cfs(0, 3) * s*s*s + cfs(0, 2) * s*s + cfs(0, 1) * s + cfs(0, 0);
    float y = cfs(1, 3) * s*s*s + cfs(1, 2) * s*s + cfs(1, 1) * s + cfs(1, 0);

    float x_dir = 3*cfs(0, 3) * s*s + 2*cfs(0, 2) * s + cfs(0, 1);
    float y_dir = 3*cfs(1, 3) * s*s + 2*cfs(1, 2) * s + cfs(1, 1);

    return Vector3f(x, y, atan2f(y_dir, x_dir));
}

Vector3f spline_point_derv(const Matrix<float, 3, 4>& cfs, double s)
{
    double x = 3*cfs(0, 3) * s*s + 2*cfs(0, 2) * s + cfs(0, 1);
    double y = 3*cfs(1, 3) * s*s + 2*cfs(1, 2) * s + cfs(1, 1);
    double z = 3*cfs(2, 3) * s*s + 2*cfs(2, 2) * s + cfs(2, 1);
    return Vector3f(x, y, z);
}

Vector3f spline_point_double_derv(const Matrix<float, 3, 4>& cfs, double s)
{
    double x = 6*cfs(0, 3) * s + 2*cfs(0, 2);
    double y = 6*cfs(1, 3) * s + 2*cfs(1, 2);
    double z = 6*cfs(2, 3) * s + 2*cfs(2, 2);
    return Vector3f(x, y, z);
}


float spline_projection_criteria(const Matrix<float, 3, 4>& cfs, float s, const Vector3f& y)
{
    float res = (y - spline_point(cfs ,s)).transpose() * spline_point_derv(cfs, s);
    return res;
}

float spline_projection_criteria_derv(const Matrix<float, 3, 4>& cfs, double s, const Vector3f& y)
{
    float res1 = -spline_point_derv(cfs, s).transpose() * spline_point_derv(cfs ,s);
    float res2 = (y - spline_point(cfs ,s)).transpose() * spline_point_double_derv(cfs ,s);
    return res1 + res2;
}

float spline_param_of_point_projection_all_spline(const Vector3f& y, const Matrix<float, 3, 4>& cfs)
{
    float eps = 1e-3;
    float slim = 1;
    float step = slim / 5.0;
    for (float s = 0; s < slim; s += step)
    {
        float x0 = s;
        float fx0 = 0;
        float fx0_dot = 0;
        for (int i = 0; i < 5; i++)
        {
            fx0 = spline_projection_criteria(cfs ,x0, y);
            fx0_dot = spline_projection_criteria_derv(cfs ,x0, y);
            x0 = x0 - fx0/fx0_dot;
        }
        if (fabs(fx0) < eps && x0 > -eps && x0 < slim+eps)
        {
            return x0;
        }
    }
    return -1;
}

bool spline_content_point(const Vector3f& y, const Matrix<float, 3, 4>& spline)
{
	Vector3f N_a = spline_point_derv(spline, 0);
	Vector3f x_a = spline_point(spline, 0);
	Vector3f N_b = spline_point_derv(spline, 1);
	Vector3f x_b = spline_point(spline, 1);
	float projection1 = N_a.dot(y-x_a);
	float projection2 = N_b.dot(y-x_b);
	return (projection1 >= 0) && (projection2 <= 0);
}


float spline_param_of_point_projection_first_order(const Vector3f& y, const Matrix<float, 3, 4> & spline)
{
	Vector3f a = spline_point(spline, 0);
	Vector3f b = spline_point(spline, 1);
	Vector3f N_a = spline_point_derv(spline, 0).normalized();
	Vector3f N_b = spline_point_derv(spline, 1).normalized();
	Matrix2f A;  A << N_a[0], N_a[1], N_b[0], N_b[1];
	float det_a = N_a[0]*N_b[1] - N_b[0]*N_a[1];
	float cosFI = N_a.dot(N_b);
	if(abs(cosFI-1.0) < 0.001 || cosFI >= 1.0f || abs(det_a) < 0.001)
	{
		float dist_ab = (a-b).norm();
		float dist_xa = (y-a).dot(N_a);
		//cout<<a<<" "<< b<<endl;
		//cout<<"spline"<<spline<<endl;
		//cout<<dist_ab<<" "<< dist_xa<<endl;
		float est_s = abs(dist_xa / dist_ab);
		return est_s;
	}
	float FI = acos(cosFI);
	float a0 = N_a.dot(a);
	float b0 = N_b.dot(b);
	Vector2f B0; B0 << a0, b0;
	Vector2f B1; B1 << a0-N_a[2], b0-N_b[2];
	Vector2f xy0 = A.inverse()*B0;
	Vector2f xy1 = A.inverse()*B1;
	Vector3f p0; p0 << xy0[0], xy0[1], 0;    //solution in z=0  ////points on  line on planes intersection
	Vector3f p1; p1 << xy1[0], xy1[1], 1;    //solution in z=1
	Vector3f p0_y_dir = y-p0;
	Vector3f p1_y_dir = y-p1;
	Vector3f N = p0_y_dir.cross(p1_y_dir).normalized();  //normal to new plane
	if(N_a.dot(N) < 0)
	{
		N = -N;
	}

	float cosfi = N_a.dot(N);
	float fi = cosfi < 1.0f ? acos(cosfi) : 0.0f;
/*	cout<<"cos fi "<<cosfi<<endl;
	cout<<"A: "<<A<<endl;
	cout<<"B0: "<<B0<<endl;
	cout<<"B1: "<<B1<<endl;
	cout<<"check point 1: "<<N_a.dot(p0-a)<<endl;
	cout<<"check point 2: "<<N_b.dot(p1-b)<<endl;
	cout<<"N_a: "<<endl<<N_a.normalized()<<endl;
	cout<<"N_b: "<<endl<<N_b.normalized()<<endl;
	cout<<"new_dir: "<<endl<<N<<endl;
	cout<<"FI: "<<FI<<endl;
	cout<<"fi: "<<fi<<endl;*/
	float est_s = fi/FI;
	return est_s;
}

float spline_param_of_point_projection(const Vector3f& y, const Matrix<float, 3, 4>& cfs)
{
    float eps = 1e-3;
	float slim = 1;
	float s_star_f = spline_param_of_point_projection_first_order(y,cfs);
	float x0 = s_star_f;
	float fx0;
	float fx0_dot;
	for(int i = 0; i < 3; i++)
	{
		fx0 = spline_projection_criteria(cfs, x0 , y);
		fx0_dot = spline_projection_criteria_derv(cfs, x0, y);
		x0 =  x0 - fx0/fx0_dot;
	}



	float f_first = spline_projection_criteria(cfs, s_star_f , y);
    //float f_sec = spline_point_derv(cfs, x0).dot(y - spline_point(cfs, x0));
    //cout<<"f_first = "<<f_first<<"  f_sec = "<<fx0<<endl;
    if(fabs(fx0) < eps && x0 > -eps && x0 < slim+eps)
    {
    	return x0;
    }
/*    cout<<"ERROR"<<" ";
    cout<<x0<<endl;*/
    return -1;
}

void find_control_params(const Vector3f& y, const std::vector<Matrix<float, 3, 4>>& spline, size_t start_int, float search_distance, ControlParams& cr)
{
	float distance = 0;
	size_t ind = start_int;
	while (distance < search_distance)
	{
		if(ind == spline.size())
		{
			break;
		}
		Vector3f p_begin = spline_point(spline[ind], 0);
		Vector3f p_end = spline_point(spline[ind], 1);
		Vector3f d = p_end-p_begin;
		distance += d.norm();
		if(spline_content_point(y, spline[ind]))
		{
			//float s_star = spline_param_of_point_projection(y,  spline[ind]);
			float s_star = spline_param_of_point_projection_all_spline(y,  spline[ind]);
			if(s_star!=-1)
			{
				fill_control_params_ok(cr, s_star, spline[ind], ind, y);
				return;
			}
		}
		ind++;
	}
	fill_control_params_not_ok(cr);
}
