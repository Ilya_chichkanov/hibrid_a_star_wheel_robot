#pragma once
#include <definitions.hpp>



Vector4 quatFromEul(const Vector3& eul);

Vector3 quatToEul(const Vector4& q);

Vector4 quatMultiply(const Vector4& q, const Vector4& r);

Vector4 quatInverse(const Vector4& q);

Vector3 quatRotate(const Vector4& q, const Vector3& v);

Eigen::Matrix<float, 3, 3> quatToMatrix(const Vector4& q);

Eigen::Matrix<float, 3, 3> crossOperator(const Vector3& v);

Vector4 quatBetweenVectors(const Vector3& v1, const Vector3& v2);

float angleBetweenVectors2d(Eigen::Vector2f v1, Eigen::Vector2f v2);