/*
 * rs_control_params.cpp
 *
 *  Created on: 21 янв. 2022 г.
 *      Author: ilia
 */

#include "rs_control_params.hpp"
#include <iostream>

inline int sgn(float val)
{
	return (float(0) < val) - (val < float(0));
}

std::ostream& operator << (std::ostream &os, const ControlParams_rs & contr_param)
{
	os<<"p "<<contr_param.p<<std::endl;
	os<<"dp "<<contr_param.dp<<std::endl;
	os<<"ddp "<<contr_param.ddp<<std::endl;
	os<<"delta "<<contr_param.delta<<std::endl;
	os<<"Delta "<<contr_param.Delta<<std::endl;
	os<<"s_star "<<contr_param.sstar<<std::endl;
	return os;
}

static void fill_param(ControlParams_rs& cr, const Vector2f& p_, const Vector2f& dp_, const Vector2f& ddp_, const Vector2f& point,  float s_star);

void fill_param(ControlParams_rs& cr, const Vector2f& p_, const Vector2f& dp_, const Vector2f& ddp_, const Vector2f& point, float s_star)
{
	cr.p = {p_[0], p_[1], 0};
	cr.dp = {dp_[0], dp_[1], 0};
	cr.ddp = {ddp_[0], ddp_[1], 0};
	Vector2f delta2 = point - p_;
	cr.Delta = {delta2[0], delta2[1], 0};
	cr.delta = cr.Delta.norm();
	cr.sstar = s_star;
}

bool check_point_belong_rs_element(const ReedsShepp_element& rs_element,  const Vector2f& point, float u_max)
{
	Vector2f x_a, N_a;
	Vector2f x_b, N_b;
	Vector2f p = {point[0], point[1]};
	rs_p_dp(rs_element, 0, u_max, x_a, N_a);
	rs_p_dp(rs_element, 1, u_max, x_b, N_b);
    float projection1 = N_a.dot(p-x_a);
    float projection2 = N_b.dot(p-x_b);
    if((rs_element.type != STRAIGHT) && (abs(rs_element.length*u_max) > M_PI))
    {
    	return (projection1*rs_element.length >= 0) || (projection2*rs_element.length <= 0);
    }
    return (projection1*rs_element.length >= 0) && (projection2*rs_element.length <= 0);

}

void find_cr_param_rs_element(const ReedsShepp_element& rs_element, const Vector2f& point, float u_max, ControlParams_rs& cr)
{
    Vector2f p, dp, ddp;
    Vector2f v_p, v_start, v_end;
    Vector2f end_point, temp2;
    rs_p_dp(rs_element, 1.0, u_max, end_point, temp2);
    Vector2f start_point = {rs_element.start_point[0], rs_element.start_point[1]};
    float sin_yaw = sin(rs_element.start_point[2]);
    float cos_yaw = cos(rs_element.start_point[2]);
    if(rs_element.type == STRAIGHT)
    {
    	Vector2f dp = {cos_yaw, sin_yaw};
    	p = start_point + dp*(dp.dot(point - start_point));
    	ddp = {0, 0};
    	v_p = p - start_point;
    	v_end = end_point - start_point;
    	float s_star = sgn(v_p.dot(v_end))*v_p.norm()/v_end.norm();
    	fill_param(cr, p, dp, ddp, point, s_star);
    	return;
    };

    Vector2f p_norm = {-sin_yaw, cos_yaw};
    Vector2f center_point;
    if(rs_element.type == RIGHT)
    {
    	center_point = start_point - p_norm/u_max;
    }
    else
    {
    	center_point = start_point + p_norm/u_max;
    }
    p = center_point + (point - center_point)/(point - center_point).norm()/u_max;
    Vector2f temp =  (p - center_point)/ (p - center_point).norm();
    dp = {-temp[1], temp[0]};
    ddp = (center_point - point)/(point - center_point).norm()/u_max;

    v_p = point - center_point;
    v_start = start_point - center_point;
    v_end = end_point - center_point;
    float cos_fi = v_p.dot(v_start)/((v_p.norm()*v_end.norm()));
    float fi = cos_fi<1.0? cos(cos_fi): 1;

    float cos_FI = v_start.dot(v_end)/(v_start.norm()*v_end.norm());
    float FI = cos_FI<1.0? cos(cos_FI): 1;
    float s_star = fi/FI;
    fill_param(cr, p, dp, ddp, point, s_star);
}


// inline float pi_to_pi(float angle)
// {
// 	if(angle > 0)
// 	{
// 		return fmod(angle + M_PI, 2*M_PI) - M_PI;
// 	}
// 	else
// 	{
// 		return M_PI - fmod(M_PI - angle , 2*M_PI) ;
// 	}
// }

// static Vector3 move(const Vector3 &pos, float d, float u)
// {
// 	float x = pos[0] + d*cos(pos[2]);
// 	float y = pos[1] + d*sin(pos[2]);
// 	float yaw = pi_to_pi(pos[2]+ d*u);
// 	return  {x, y, yaw};
// }

static void make_trajectory(std::vector<ReedsShepp_element>& trajectory, const Vector3& start_point, float u_max)
{
	Vector3 pos = start_point;

	for (auto& elem :trajectory)
	{
		elem.start_point = pos;
		pos = rs_expand(elem, u_max, 1.0);

	}
}

void test_rs_elements()
{
	Vector3 start_point = {-16.5, 12, 70*M_PI/180};
	float u_max = 0.5;
	Vector2f point;
	int ind;

	point = {-14,  12};
	ind = 1;

//	point = {-15,  9.4};
//	ind = 1;
//
//	point = {-18,  9.0};
//	ind = 2;
//
//	point = {-18,  9.0};
//	ind = 3;
//
//	point = {-19,  13.0};
//	ind = 4;
//
//	point = {-19,  13.0};
//	ind = 4;
//
//	point = {-16,  15.0};
//	ind = 5;

	std::vector<ReedsShepp_element> trajectory = {
			{RIGHT, 2, {0, 0, 0}},
			{RIGHT, 2, {0, 0, 0}},
			{STRAIGHT, 3, {0, 0, 0}},
			{LEFT, 1.3, {0, 0, 0}},
			{RIGHT, -4, {0, 0, 0}},
			{LEFT, -7, {0, 0, 0}},
			{STRAIGHT, -3, {0, 0, 0}},
	};

	make_trajectory(trajectory,start_point, u_max);
	auto rs_element = trajectory[ind];
	//Vector2f point = {trajectory[1].start_point[0], trajectory[1].start_point[1]};
	if(check_point_belong_rs_element(rs_element, point, u_max))
	{
		std::cout<<"belong"<<std::endl;
	}
	else
	{
		std::cout<<"do not belong"<<std::endl;
	}
	ControlParams_rs cr;
    find_cr_param_rs_element(rs_element, point, u_max, cr);
    std::cout<<"start rs_test"<<std::endl;

	std::cout<<cr<<std::endl;


//	std::array<float,6> s_list= {0.1, 0.2, 0.3, 0.4, 0.5, 1.0};
//	Vector2 p_end, dp_end;
//	rs_element = trajectory[0];
//	cout<<"start rs_p_dp_test"<<std::endl;
//	for (auto s: s_list)
//	{
//		rs_p_dp(rs_element, s, u_max, p_end, dp_end);
//		std::cout<<p_end[0]<<" "<<p_end[1]<<std::endl;
//		std::cout<<dp_end[0]<<" "<<dp_end[1]<<std::endl;
//		std::cout<<"------------------"<<std::endl;
//	}


}
