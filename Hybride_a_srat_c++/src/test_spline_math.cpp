#include "definitions.hpp"
#include "spline_math.hpp"
#include <iostream>
#include <vector>

std::ostream& operator << (std::ostream &os, const ControlParams &contr_param)
{
    os << std::endl << "ok:\n" << contr_param.ok << std::endl;
    os << std::endl << "cfs:\n" << contr_param.cfs << std::endl;
    os << std::endl << "cfs_index:\n" << contr_param.cfs_index << std::endl;
    os << std::endl << "sstar:\n" << contr_param.sstar << std::endl;
    os << std::endl << "p:\n" << contr_param.p << std::endl;
    os << std::endl << "dp:\n" << contr_param.dp << std::endl;
    os << std::endl << "ddp:\n" << contr_param.ddp << std::endl;
    os << std::endl << "Delta:\n" << contr_param.Delta << std::endl;
    return os << std::endl << "delta:\n" << contr_param.delta << std::endl;
}

ControlParams control_params(const Vector3f& y, const std::vector<Matrix<float, 3, 4>>& spline)
{
    ControlParams cr;
    Matrix<float, 3, 4> cfs; 
    float sstar = -1;
    for (int i = 0; i < spline.size(); i++)
    {
        cfs =  spline.at(i);
        float s  = spline_param_of_point_projection(y, cfs);
        if(s > 0) 
        {
            fill_control_params_ok(cr, s, cfs, i, y);
            return cr;
        }
    }
    fill_control_params_not_ok(cr);
    return cr;
}

int main(int argc,char** argv)
{
    
    // waypoints
    std::vector<Vector3f> set;
    
    set.push_back(Vector3f(0,0,0));
    set.push_back(Vector3f(1,2,3));
    set.push_back(Vector3f(1,2,5));
    set.push_back(Vector3f(1,4,6));
    set.push_back(Vector3f(1,8,5));
    
    // spline
    std::vector<Matrix<float, 3, 4>> spline = M_spline_from_set(set);
    for (int i = 0; i < spline.size(); i++)
    {
//        std::cout << std::endl << spline.at(i) << std::endl;
    }
//    return 0;
    
    // sstar and pstar
    Vector3f pos(9, -8, -2.8);
    /*
    Matrix<float, 3, 4> cfs; 
    float sstar = -1;
    for (int i = 0; i < spline.size(); i++)
    {
        cfs =  spline.at(i);
        double s  = spline_param_of_point_projection(pos, cfs);
        if(s>0){
            sstar = s;
            //Delta = pos - getSplinePoint(cfs, sstar);
            //delta = Delta.norm();
            break;
        }
    }
    
    if (sstar < 0){
        std::cout << "BAD CURVE DISTANCE CALCULATION" << std::endl;
        return 0;
    }
    
    //std::cout << std::endl << sstar << std::endl;
    //std::cout << std::endl << spline_point(cfs, sstar) << std::endl;
    */
    
    // ctrl params
    ControlParams cr = control_params(pos, spline);
    //cr.cout();
    std::cout<<"control params:"<<std::endl<<cr;
    return 0;
}
