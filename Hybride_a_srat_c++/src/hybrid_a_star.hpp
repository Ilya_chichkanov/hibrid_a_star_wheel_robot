/*
 * hybrid_a_star.hpp
 *
 *  Created on: 24 нояб. 2021 г.
 *      Author: ilia
 */

#ifndef HYBRID_A_STAR_HPP_
#define HYBRID_A_STAR_HPP_

#include "cmath"
#include <iostream>
#include <array>
#include <unordered_map>
#include <functional>
#include <queue>
#include "reeds_shepp.h"
#include "KDTree.hpp"
#include "rs_elements_math.h"

namespace Planner_a_star
{
#define STEP_LENGTH          ((float)0.6)
#define XY_GRID_RESOLUTION   0.4  // [m]
#define YAW_GRID_RESOLUTION  (5*M_PI/180.0)


#define SB_COST              30.0 // switch back penalty cost
#define BACK_COST            3.0  // backward penalty cost
#define STEER_CHANGE_COST    0.2  // u angle change penalty cost
#define STEER_COST           0.0  // u angle change penalty cost

#define MAX_ITER             10000

using Queue = std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>, std::greater<std::pair<float, int>>>;


struct Node
{
	Node():
		x_ind(0),
		y_ind(0),
		yaw_ind(0),
		parent_index(-1),
		u(0),
		cost(0),
		dir(true)
	{
	};

	Node( const Vector3& pos_, bool dir_):
	parent_index(-1),
	u(0),
	cost(0)
	{
		dir = dir_;
		pose = pos_;
		x_ind = pos_[0] / XY_GRID_RESOLUTION;
		y_ind = pos_[1] / XY_GRID_RESOLUTION;
		yaw_ind = pos_[2] / YAW_GRID_RESOLUTION;
	};

    void reinit( int x_ind_, int y_ind_, int yaw_ind_,
				 float u_, int parent_index_, float cost_,
				 const Vector3& pose_,
				 bool dir_ )
    {
       x_ind = x_ind_;
       y_ind = y_ind_;
	   yaw_ind = yaw_ind_;
	   u = u_;
	   parent_index = parent_index_;
	   cost = cost_;
	   pose = pose_;
	   dir = dir_;
    }

    int x_ind;
    int y_ind;
    int yaw_ind;
    int parent_index;
    float u;
    float cost;
    bool dir;
    Vector3 pose;


};

using NodeList =  std::unordered_map<int, Node>;

class Config
{
public:
	Config(){
		Vector3 c_p = {0,0,0};
		border_size = 15;
		reinit(c_p);
	};
	void init( float border_size_)
	{
		border_size = border_size_;
	};
    void reinit(const Vector3& center_point)
    {
		min_x = round((center_point[0] - border_size)/ XY_GRID_RESOLUTION);
		max_x = round((center_point[0] + border_size)/ XY_GRID_RESOLUTION);
		min_y = round((center_point[1] - border_size)/ XY_GRID_RESOLUTION);
		max_y = round((center_point[1] + border_size)/ XY_GRID_RESOLUTION);

		x_w = round(max_x - min_x);
		y_w = round(max_y - min_y);

		min_yaw = round(-M_PI / YAW_GRID_RESOLUTION);
		max_yaw = round(M_PI / YAW_GRID_RESOLUTION);
		yaw_w = round(max_yaw - min_yaw);
    }
	int min_x;
	int min_y;
	int max_x;
	int max_y;
	int min_yaw;
	int max_yaw;
	int x_w;
	int y_w;
	int yaw_w;
	float border_size;
	void print()
	{
		std::cout<<"x_w: "<<x_w<<std::endl;
		std::cout<<"y_w: "<<y_w<<std::endl;
		std::cout<<"yaw_w: "<<yaw_w<<std::endl;
	}
};

class Planner
{
public:
	Planner();

	void reinit(float border_size_, float obstcl_r_, float u_max_, float car_length_, float car_width_);

	bool a_star_search(const Vector3& start_point_,  const std::vector<Vector3> goal_points, const KDTree& obstcl_tree, Path_RS & path);

	bool check_car_collision(const std::vector<Vector3>& pos_list, const KDTree& obstcl_tree) const;

private:

	inline bool verify_index(const Node & node) const;

	inline int  calc_index(const Node & node) const;

	bool calc_next_node(const Node& current, float u, bool dir,  Node& next_node, const KDTree& obstcl_tree, bool check_collision) const;

	std::vector<Node> get_neighbors(const Node& current, const KDTree& obstcl_tree, bool check) const;

	void make_final_path(const NodeList& close_list, const Path_RS& final_path, int start_ind, int curr_ind, Path_RS& path) const;

	float calc_rs_path_cost(const Path_RS& path) const;

	Path_RS analitical_expansion(const Vector3f& start, const std::vector<Vector3f> & goal_list, const KDTree& obstcl_tree) const;

	float calc_cost(const Node& current_node, const Node& goal_node);

	ReedsSheppStateSpace rs_state;
	std::unordered_map<int,float> heuvr_f;
	float _u;
	float car_length;
	float car_width;
	float obstcl_r;
	float analit_exp_dist;
	mutable bool start_search;
	mutable std::unordered_map<int,bool> check_colision_list;
	mutable std::unordered_map<int,bool> check_colision_list2;
	Config config;
};
}
#endif /* HYBRID_A_STAR_HPP_ */
