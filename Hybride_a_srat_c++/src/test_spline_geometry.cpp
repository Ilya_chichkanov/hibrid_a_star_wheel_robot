#include "definitions.hpp"
#include "spline_math.hpp"
#include <iostream>
#include <vector>
#include "csv.h"
#include <cassert>
#include <array>
#include <map>
#include <queue>
using namespace std;
using Queue = std::priority_queue<std::pair<float, int>, std::vector<std::pair<float, int>>, std::greater<std::pair<float, int>>>;




template <typename T>
T cutMinMax(T v, T min_val, T max_val)
{
    return fmin(fmax(v, min_val), max_val);
};

int loadWaypoints(const std::string& ros_param_route_file_name, std::vector<Eigen::Vector3f>& waypoints)
{
    int ret = 0;

    try
    {
       io::CSVReader<3> in(ros_param_route_file_name);
       in.read_header(io::ignore_extra_column, "x", "y", "z");

        double x;
        double y;
        double z;

        while(in.read_row(x, y, z))
        {
            Eigen::Vector3f p(x,y,z);
            waypoints.emplace_back(p);
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << " a standard exception was caught, with message '"<< e.what() << "'\n";
        ret = -1;
    }
    return ret;
}



int main(int argc,char** argv)
{
	unsigned int  rand_ind = time(0);//1635362232;
	srand(rand_ind);
	cout<<"ind: "<<rand_ind<<endl;
    std::vector<Vector3f>  set;
    loadWaypoints("surf_waypoints/car3.csv",set); //july6_car2.csv
    std::vector<Matrix<float, 3, 4>> spline = M_spline_from_set(set);
    size_t spline_size = spline.size();
    cout<<"spline size = "<<spline_size<<endl;
    int serching_ind_range = 8;
    ControlParams cr;
    for(int target_ind=0; target_ind<spline_size; target_ind++){
		Vector3f dir = spline_point_derv(spline[target_ind], 0);
		Vector3f pos = set.at(target_ind)+0.5*dir+1.9*Vector3f::Random(3,1);
		int cfs_ind = target_ind-serching_ind_range;
		cfs_ind = cutMinMax(cfs_ind,0,(int)spline_size);
		find_control_params(pos, spline, cfs_ind, 20, cr);

		for (;cfs_ind<target_ind+serching_ind_range;cfs_ind++)
		{
			bool res = spline_content_point(pos, spline[cfs_ind]);
			if(res){
				//assert(cfs_ind==target_ind);
				assert(abs(cfs_ind-target_ind)<6);
				float s_est_first = spline_param_of_point_projection_first_order(pos, spline[cfs_ind]);
				float s_est_second = spline_param_of_point_projection(pos,spline[cfs_ind]);
				float s_calc = spline_param_of_point_projection_all_spline(pos,spline[cfs_ind]);
				cout<<"s'star first order:   "<<s_est_first<<endl;
				cout<<"s'star from s*:       "<<s_est_second<<endl;
				cout<<"s'star numerical:     "<<s_calc<<endl;
				cout<<"s'star numerical2:    "<<cr.sstar<<endl;
				cout<<"--------------------"<<endl;
				assert((s_calc>0)&&(s_calc<1));
				assert(cr.ok!=false);
				//assert((s_est_first>0)&&(s_est_first<1));
				assert((s_est_second>0)&&(s_est_second<1));
				assert(s_calc!=-1);
				assert(s_est_first!=-1);
				assert(s_est_second!=-1);
				assert(abs(s_est_first-s_calc)<0.2);
				assert(abs(s_est_second-s_calc)<0.001);
				break;
			}
		}
	}
    cout<<"END"<<endl;
    cout<<Vector3f::Random(3,1)<<endl;
    cout<<"ind: "<<rand_ind<<endl;
    cout<<"good !: "<<rand_ind<<endl;
	Queue heap;
	heap.push(std::make_pair(0.4, 10));
	heap.push(std::make_pair(0.3, 20));
	heap.push(std::make_pair(0.6, 15));
	heap.push(std::make_pair(0.1, 100));
	heap.push(std::make_pair(13.4, 22));
	auto elem = heap.top();
	heap.pop();
    cout<<elem.first<<" "<<elem.second<<endl;

	auto elem1 = heap.top();

	cout<<elem1.first<<" "<<elem1.second<<endl;

	map<int, int> a;
	a[1] = 23;
	a[34] = 3;
	auto p = a.at(1);
	cout<<p;
    return 0;
}
