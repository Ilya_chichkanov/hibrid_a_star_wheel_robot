#include "definitions.hpp"
#include "spline_math.hpp"
#include <iostream>
#include "csv.h"
#include <cassert>
#include "hybrid_a_star.hpp"
#include <fstream>
#include "profile.hpp"
#include "KDTree.hpp"
#include "reeds_shepp.h"
#include <string>
#include "rs_elements_math.h"
#include "rs_control_params.hpp"
using namespace std;

std::ostream& operator << (std::ostream &os, const Vector3 &vec)
{
	os<<vec[0]<<" "<<vec[1]<<" "<<vec[2];
	return os;
}

template <typename T>
T cutMinMax(T v, T min_val, T max_val)
{
    return fmin(fmax(v, min_val), max_val);
};


int loadWaypoints(const std::string& ros_param_route_file_name, std::vector<Eigen::Vector3f>& waypoints)
{
    int ret = 0;

    try
    {
       io::CSVReader<3> in(ros_param_route_file_name);
       in.read_header(io::ignore_extra_column, "x", "y", "z");

        double x;
        double y;
        double z;

        while(in.read_row(x, y, z))
        {
            Eigen::Vector3f p(x,y,0*z);  ////z = 0
            waypoints.emplace_back(p);
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << " a standard exception was caught, with message '"<< e.what() << "'\n";
        ret = -1;
    }
    return ret;
}


std::vector<Vector3f> make_points_from_relative_data(std::vector<Vector3f> points_list, Matrix<float, 3, 4> & spline_cfg)
{
	std::vector<Vector3f>  new_points;
	Matrix<float, 3, 3> rot;
	rot << Matrix<float, 3, 1>(0, -1, 0), Matrix<float, 3, 1>(1, 0, 0), Matrix<float, 3, 1>(0, 0, 1);
	Vector3f x0 = spline_point(spline_cfg, 0);
	Vector3f mov_dir = spline_point_derv(spline_cfg,0);
	Vector3f dir1_ortgn = rot.transpose()*mov_dir;
	mov_dir.normalize();
	dir1_ortgn.normalize();
	for (const auto & point: points_list)
	{
		Vector3f x_new = x0 + point[1]*dir1_ortgn + point[0]*mov_dir;
		x_new[2] = 0;
		new_points.push_back(x_new);
	}
	return new_points;
}

void test_a_star()
{
	using namespace Planner_a_star;
    std::vector<Vector3f>  set;
    loadWaypoints("surf_waypoints/car3.csv",set); //july6_car2.csv
    std::vector<Matrix<float, 3, 4>> spline = M_spline_from_set(set);
    size_t spline_size = spline.size();
    cout<<"spline size = "<<spline_size<<endl;
	std::vector<Vector3f> obstcl_list = \
	{
			{0.01, 0.5, 0},
			{3.1, -0.8, 0},
			{1.0,-1.9, 0},
			{1.0, -2.6, 0},
			{-1.0, -5.0, 0},
			{-2,-4, 0},
			{0, -4.7, 0},
			{2,-4, 0},
			{4,-2, 0},
			{0,-0.3, 0},
			{-1.6,1.5, 0},
			{-3,2, 0},
			{-5,-4, 0},
			{1.7,-4.3, 0},
			{ -4, -4, 0},
			{-4.4,2, 0},
			{2.2,-1.2, 0},
			{1.8,-0.5, 0}
	};


	obstcl_list = make_points_from_relative_data(obstcl_list, spline[23]);
	//obstcl_list.erase(obstcl_list.begin() + 7, obstcl_list.end());

	pointVec points;
	for (const auto &elem: obstcl_list)
	{
		points.push_back({elem[0], elem[1]});
	}
	KDTree obstcl_tree = KDTree(points);
	points.clear();

	Vector3f start = {-17.5, 13.0, 140*M_PI/180};
	//Vector3f start = {-56.1006, -20.7202, 1.16315};

	Path_RS path;
	float u_max = 0.5;
	bool res;
	{
		LOG_DURATION("hybrid_a_star");
		Planner plan;
		float border_size = 200;
		float car_length = 0.725;
		float car_wigth = 0.3;
		float obstcl_r = 1;
		plan.reinit(border_size,obstcl_r, u_max, car_length, car_wigth);
		std::vector<Vector3> goal_points;
		int goal_spline_ind = 27;
		for(int ii = 3; ii < 10; ii++)
		{
			goal_points.push_back(spline_2d_state(spline[goal_spline_ind + ii], 0));
		}

//		goal_points.clear();
//		goal_points.push_back({-19.5, 12.0, 0*M_PI/180});
		res = plan.a_star_search(start, goal_points, obstcl_tree, path);
	}

	cout<<"finish number of points is "<< path.pose_list.size() << endl;
	cout<<"path length is "<< path.total_length << endl;
	if(res)
	{
		rs_path_seed(0.05, u_max, path);
		ofstream myfile ("result_c++.txt");
		if (myfile.is_open())
		{
			for(size_t ii = 0; ii < path.pose_list.size(); ii ++)
			{
				myfile << path.pose_list[ii] << endl;
			}
			myfile.close();
		}
		else
		{
			cout << "Unable to open file";
		}
		cout<<path.element_list.size()<<endl;
		ofstream point_file ("points.txt");
		point_file.is_open();

		for(size_t ii = 0; ii < path.element_list.size(); ii++)
		{
			auto elem_rs = path.element_list[ii];
			string type;
			switch (elem_rs.type)
			{
				case STRAIGHT:
					type = "S";
					break;
				case RIGHT:
					type = "R";
					break;
				case LEFT:
					type = "L";
					break;
			}

			cout <<type<<" ("<<elem_rs.length<<") <"<< elem_rs.start_point<<">    ";
			point_file << elem_rs.start_point << endl;
		}
		cout<<endl;
		point_file.close();
	}
	else std::cout<<"finish fail"<<std::endl;

	std::cout<<"End"<<std::endl;
/*	ReedsSheppPath rs;
	rs.test();*/
	//double max_curv = tan(0.6)/0.75;
	//ReedsSheppStateSpace rs(max_curv);

	//rs.test();
}

int main(int argc,char** argv)
{
	test_a_star();
	//test_rs_elements();

    return 0;
}
