#include <rs_elements_math.h>
#include <cmath>
#include <string>
#include "reeds_shepp.h"
using namespace std;


inline int sgn(float val)
{
	return (float(0) < val) - (val < float(0));
}

void rs_p_dp(const ReedsShepp_element& rs_element, float s_star, float u_max, Vector2f& p, Vector2f& dp)
{
    Vector2f start_point = {rs_element.start_point[0], rs_element.start_point[1]};
    float sin_yaw = sin(rs_element.start_point[2]);
    float cos_yaw = cos(rs_element.start_point[2]);
    Vector2f p_derv_start = {cos_yaw, sin_yaw};
    if(rs_element.type == STRAIGHT)
    {

    	dp = p_derv_start;
        p =  start_point + dp*rs_element.length*s_star;
        return;
    }
    Vector2f p_norm = {-sin_yaw, cos_yaw};
    float FI = abs(rs_element.length*u_max);
    float  u = rs_element.type == RIGHT ? u_max: -u_max;
    float fi = FI*s_star;
    Vector2f center_point;
    if(rs_element.type == RIGHT)
    {
    	fi = -sgn(rs_element.length)*fi;
    }
    else if(rs_element.type == LEFT)
    {
    	fi = sgn(rs_element.length)*fi;
    }

    center_point = start_point - p_norm/ u;
    Matrix<float, 2, 2> rot;
    rot << Eigen::Matrix<float, 2, 1>(cos(fi), sin(fi)), Eigen::Matrix<float, 2, 1>(-sin(fi), cos(fi));
    p =  center_point + rot*p_norm/u;
    dp = rot*p_derv_start;
}

Path_RS rs_make_path(const Vector3f& start, const Vector3f& goal,  float u_max, float step_size)
{
	Path_RS lattice_path;
	ReedsSheppStateSpace rs_state;
	float rho = 1.0/u_max;
	rs_state.reinit(rho);
	float q0[3] = {start[0], start[1], start[2]};
	float q1[3] = {goal[0], goal[1], goal[2]};
	ReedsSheppStateSpace::ReedsSheppPath path = rs_state.reedsShepp(q0, q1);

	float dist = path.length()*rho;
	lattice_path.total_length = dist;
    for (float seg = 0.0; seg <= dist; seg += step_size)
    {
        float qnew[3] = {};
        rs_state.interpolate(q0, path, seg/rho, qnew);
        lattice_path.pose_list.push_back({qnew[0], qnew[1], qnew[2]});
    }
    SegmentType sg_type;
    Vector3 point = {q0[0], q0[1], q0[2]};
    float seg_length = 0.0;
    for (int i=0;i<5;++i)
    {
    	switch (path.type_[i]){
		case ReedsSheppStateSpace::RS_STRAIGHT:
			sg_type = STRAIGHT;
			break;
		case ReedsSheppStateSpace::RS_RIGHT:
			sg_type = RIGHT;
			break;
		case ReedsSheppStateSpace::RS_LEFT:
			sg_type = LEFT;
			break;
		case ReedsSheppStateSpace::RS_NOP:
			break;
    	}
    	float length = path.length_[i];
    	if(path.type_[i] != ReedsSheppStateSpace::RS_NOP)
    	{
    		lattice_path.element_list.push_back({sg_type, length*rho, point});
    		float qnew[3] = {};
    		seg_length += abs(length);
    		rs_state.interpolate(q0, path, seg_length, qnew);
    		point[0] = qnew[0];
    		point[1] = qnew[1];
    		point[2] = qnew[2];
    	}
    }
    return lattice_path;
}
Vector3 rs_expand(const ReedsShepp_element& rs_elem, float u_max, float s)
{
	Vector2 p_end, dp_end;
	rs_p_dp(rs_elem, s, u_max, p_end, dp_end);
	float fi = atan2f(dp_end[1], dp_end[0]);
	return {p_end[0], p_end[1], fi};
}

void rs_path_seed(float resolution, float u_max, Path_RS& path)
{
	path.pose_list.clear();
	Vector3 pose;
	for(const auto& rs_elem: path.element_list)
	{
		float l = 0;
		float s = 0;
		while(l<abs(rs_elem.length))
		{
			s = l/abs(rs_elem.length);
			pose = rs_expand(rs_elem, u_max, s);
			l+=resolution;
			path.pose_list.push_back(pose);
		}
	}
}

