#include "hybrid_a_star.hpp"
#include <algorithm>
#include <iterator>
#include <iostream>
namespace Planner_a_star
{


Planner::Planner():
_u(0.5)
,car_length(0.725)
,car_width(0.4)
,obstcl_r(1.0)
,analit_exp_dist(2.5)
,start_search(true)
{};

void Planner::reinit(float border_size_, float obstcl_r_, float u_max_, float car_length_, float car_width_)
{
	config.init(border_size_);
	//config.print();
	_u = u_max_;
	car_length = car_length_;
	car_width = car_width_;
	float turning_r = 1.0/u_max_;
	rs_state.reinit(turning_r);
	obstcl_r = obstcl_r_;
};

inline bool Planner::verify_index(const Node & node) const
{
    if((config.min_x <= node.x_ind) && (node.x_ind <= config.max_x) && (config.min_y <= node.y_ind) && (node.y_ind <= config.max_y))
    {
    	return true;
    }
    return false;
}

inline int Planner::calc_index(const Node & node) const
{
    int ind = (node.yaw_ind - config.min_yaw) * config.x_w * config.y_w + \
              (node.y_ind - config.min_y) * config.x_w + (node.x_ind - config.min_x);
    return ind;
}

bool Planner::calc_next_node(const Node& current, float u, bool dir,  Node& next_node, const KDTree& obstcl_tree, bool check_collision) const
{
	std::vector<Vector3> pose_list;
	float d = dir? 1.0 : -1.0;
	SegmentType s_type = u==0? STRAIGHT: u>0? RIGHT: LEFT;
	ReedsShepp_element rs_elem = {s_type, d*STEP_LENGTH, current.pose};

	Vector3 pos = rs_expand(rs_elem, _u, 1.0);
	pose_list.push_back(pos);
	int x_ind = round(pos[0]/ XY_GRID_RESOLUTION);
	int y_ind = round(pos[1]/ XY_GRID_RESOLUTION);
	int yaw_ind = round(pos[2]/ YAW_GRID_RESOLUTION);
//	if(check_car_collision(pose_list, obstcl_tree))
//	{
//		//check_colision_list[ind] = false;
//		return false;
//	}
	if(check_collision)
	{

		//int ind = (y_ind - config.min_y) * config.x_w + (x_ind - config.min_x);
		int ind = (yaw_ind - config.min_yaw) * config.x_w * config.y_w +  (y_ind - config.min_y) * config.x_w + (x_ind - config.min_x);
		if(check_colision_list.find(ind) == check_colision_list.end())
		{
			if(check_car_collision(pose_list, obstcl_tree))
			{
				check_colision_list[ind] = false;
				return false;
			}
			else
			{
				check_colision_list[ind] = true;
			}
		}
		else
		{
			if(check_colision_list.at(ind) == false)
			{
				return false;
			}
		}
	}

    float added_cost = 0.0;

    if (dir != current.dir &&  current.dir == true && start_search == false)
    {
        added_cost += SB_COST*STEP_LENGTH;
    }

    if(dir == false)
    {
         added_cost += BACK_COST * STEP_LENGTH;
    }

    // u penalty
    added_cost += STEER_COST * STEP_LENGTH*abs(u);

    // u change penalty
    added_cost += STEER_CHANGE_COST * STEP_LENGTH * abs(current.u - u);

    float cost = current.cost + added_cost + STEP_LENGTH;

	int parent_index = calc_index(current);

	next_node.reinit(x_ind, y_ind, yaw_ind, u, parent_index, cost, pos, dir);

	return true;
}

std::vector<Node> Planner::get_neighbors(const Node& current, const KDTree& obstcl_tree, bool check) const
{
	std::vector<float> u_array = {0};
	if(current.u == 0)
	{
		u_array.push_back(-_u);
		u_array.push_back(_u);
	}
	else
	{
		u_array.push_back(current.u);
	}

	std::vector<Node> node_neigbors;
	Node new_node;

	for (auto d:  {true, false})
	{
		for (const auto& u: u_array)
		{
			if(d!= current.dir && u == current.u)
			{
				continue;
			}
			bool res = calc_next_node(current, u, d, new_node, obstcl_tree, check);
			if(res && verify_index(new_node))
			{
	        	node_neigbors.push_back(new_node);
			}
		}
	}
    return node_neigbors;
}

void Planner::make_final_path(const NodeList& close_list, const Path_RS& final_path, int start_ind, int curr_ind, Path_RS& path) const
{
	Node cur_node, prev_node;
	int nid = curr_ind;
	ReedsShepp_element rs_element;
	cur_node = close_list.at(nid);
	prev_node = cur_node;
	rs_element.type = (cur_node.u == 0)?  STRAIGHT: (cur_node.u > 0)? RIGHT: LEFT;
	rs_element.length = 0;
	path.total_length = 0;
	while(1)
	{
		cur_node = close_list.at(nid);
		int d = cur_node.dir? 1: -1;
		if(nid == start_ind)
		{
			path.pose_list.push_back(cur_node.pose);
			rs_element.start_point = cur_node.pose;
			path.element_list.push_back(rs_element);
			break;
		}
		else
		{
			Vector3 pos = cur_node.pose;
			path.pose_list.push_back(pos);
			if((cur_node.dir == prev_node.dir) && (cur_node.u == prev_node.u) && abs((rs_element.length))<2.0)
			{
				rs_element.length += d*STEP_LENGTH;
			}
			else
			{
				rs_element.start_point = cur_node.pose;
				path.element_list.push_back(rs_element);
				rs_element.type = (cur_node.u == 0)?  STRAIGHT: (cur_node.u > 0)? RIGHT: LEFT;
				rs_element.length = d*STEP_LENGTH;
			}

		}
		prev_node = cur_node;
		nid = cur_node.parent_index;
		path.total_length+=STEP_LENGTH;
	}
	std::reverse(path.pose_list.begin(), path.pose_list.end());
	std::reverse(path.element_list.begin(), path.element_list.end());

	for(const auto& pose: final_path.pose_list)
	{
		path.pose_list.push_back(pose);
	}

	for(auto it = final_path.element_list.begin(); it != final_path.element_list.end(); it++)
	{
		path.element_list.push_back(*it);
	}
	path.total_length+=final_path.total_length;
}

float Planner::calc_rs_path_cost(const Path_RS& path) const
{
	float cost = path.total_length;
	for (const auto & elem: path.element_list)
	{
		if(elem.type!=STRAIGHT)
		{
			cost+=abs(elem.length)*STEER_COST;
		}

		if(elem.length < 0)
		{
			cost+=abs(elem.length)*BACK_COST;
		}
	}
	return cost;
}

Path_RS Planner::analitical_expansion(const Vector3f& start, const std::vector<Vector3f> & goal_list, const KDTree& obstcl_tree) const
{
	std::vector<Path_RS> paths;
	Path_RS final_path;
	for (const auto & goal: goal_list)
	{
		Path_RS path = rs_make_path(start, goal,_u, 0.5);
		if(check_car_collision(path.pose_list, obstcl_tree) == false)
		{
			paths.push_back(path);
		}
	}
	if(paths.empty())
	{
		return final_path;
	}
	int best_path_index = -1;
	float minL = std::numeric_limits<float>::infinity();
	for (size_t i=0; i < paths.size(); i++) {
		float path_cost = calc_rs_path_cost(paths[i]);
		if (path_cost <= minL) {
			minL = path_cost;
			best_path_index = i;
		}
	}
	final_path = paths[best_path_index];
	return final_path;
}

float Planner::calc_cost(const Node& current_node, const Node& goal_node)
{
	 //int ind = (current_node.y_ind - config.min_y) * config.x_w + (current_node.x_ind - config.min_x);
	 int ind = calc_index(current_node);
	 if(heuvr_f.find(ind) == heuvr_f.end())
	 {
		float q0[3] = {current_node.pose[0], current_node.pose[1], current_node.pose[2]};
		float q1[3] = {goal_node.pose[0], goal_node.pose[1], goal_node.pose[2]};
		//const float* q0 = current_node.pose.data();
		//const float* q1 = goal_node.pose.data();
		heuvr_f[ind] =   rs_state.distance(q0, q1);
	 }
	 return current_node.cost + heuvr_f.at(ind);
}


bool Planner::check_car_collision(const std::vector<Vector3>& pos_list, const KDTree& obstcl_tree) const
{
	for(const auto &pos: pos_list)
	{
    	float cos_fi = cos(pos[2]);
    	float sin_fi = sin(pos[2]);
    	//suppose that car is combination of 2 ball
    	point_t point1 = {pos[0]+ cos_fi*car_width/2, pos[1] + sin_fi*car_width/2};
    	point_t point_nearest1 =  obstcl_tree.nearest_point(point1);
    	if(point_nearest1.size()==0)
    	{
    		return false; ///no obstacles
    	}
    	float dist1 = pow(point1.at(0) - point_nearest1.at(0), 2) + pow(point1.at(1) - point_nearest1.at(1), 2);
    	if(dist1 < (obstcl_r+car_width/2)*(obstcl_r+car_width/2))
		{
    		return true;
		}
    	point_t point2 = {pos[0]+cos_fi*(car_length - car_width/2), pos[1]+sin_fi*(car_length - car_width/2)};
    	point_t point_nearest2 =  obstcl_tree.nearest_point(point2);
    	float dist2 = pow(point2.at(0) - point_nearest2.at(0), 2) + pow(point2.at(1) - point_nearest2.at(1), 2);
    	if(dist2 < (obstcl_r+car_width/2)*(obstcl_r+car_width/2))
		{
    		return true;
		}
	}
	return false; ///no collision
}

bool Planner::a_star_search(const Vector3& start_point,  const std::vector<Vector3> goal_points, const KDTree& obstcl_tree, Path_RS & path)
{
	path.clear();
	heuvr_f.clear();
	check_colision_list.clear();
	start_search = true;
	config.reinit(start_point);
	Node start_node(start_point, true);
	Vector3 goal = goal_points.front();
	Node goal_node(goal, true);
	NodeList openList, closedList;
	Queue heap;
	int start_ind = calc_index(start_node);
	heap.push(std::make_pair(calc_cost(start_node, goal_node), start_ind));
	openList[start_ind] = start_node;
	int iter = 0;
	Node current_node;
	while(heap.size() > 0)
	{
		iter++;
		int current_id = heap.top().second;
		heap.pop();
		if(openList.find(current_id) != openList.end())
		{
			current_node = openList.at(current_id);
			openList.erase(current_id);
			closedList[current_id] = current_node;
		}
		else{
			std::cout << "kakogo chorta" << std::endl;
			continue;
		}
		Vector3 pos_cur = current_node.pose;
		if((pos_cur[0] - goal[0])*(pos_cur[0] - goal[0]) + (pos_cur[1] - goal[1])*(pos_cur[1] - goal[1])  < (analit_exp_dist*analit_exp_dist) )
		{
			Path_RS final_rs_path = analitical_expansion(pos_cur, goal_points, obstcl_tree);
			if(!final_rs_path.pose_list.empty())
			{
				std::cout << "find rougt!  " << "iter = " << iter << std::endl;
				std::cout << " len of close list = " << closedList.size() << " len of open list = " << openList.size() << std::endl;
				if(iter > 1)
				{
					make_final_path(closedList, final_rs_path, start_ind, current_id, path);
				}
				else
				{
					path = final_rs_path;
				}
				heuvr_f.clear();
				check_colision_list.clear();
				return true;
			}
		}

		bool check = false;
		int ind = (current_node.y_ind - config.min_y) * config.x_w + (current_node.x_ind - config.min_x);
		if(check_colision_list2.find(ind) == check_colision_list2.end())
		{
			//consider center point
			point_t point = {current_node.pose[0] + cos(current_node.pose[2])*car_length/2, current_node.pose[1] + sin(current_node.pose[2])*car_length/2};
			point_t point_nearest =  obstcl_tree.nearest_point(point);
			if(point_nearest.size() > 1)
			{
				float dist1 = pow(point[0] - point_nearest[0], 2) + pow(point[1] - point_nearest[1], 2);
				check = (dist1 > pow(obstcl_r + car_length/2 + STEP_LENGTH, 2))? false: true;
				check_colision_list2[ind] = check? 1: 0;
			}
		}
		else
		{
			check = check_colision_list2.at(ind);
		}

		for (const auto & neighbor : get_neighbors(current_node, obstcl_tree, check))
		{
			int neighbor_index = calc_index(neighbor);
			if(closedList.find(neighbor_index) != closedList.end())
			{
				continue;
			}

			bool in_open_list = openList.find(neighbor_index) != openList.end();
			bool better_way_found = in_open_list ? openList.at(neighbor_index).cost > neighbor.cost: false;
			if(!in_open_list or better_way_found )
			{
				float new_cost = calc_cost(neighbor, goal_node);
				heap.push(std::make_pair(new_cost, neighbor_index));
				openList[neighbor_index] = neighbor;
			}
		}

	    if(start_search)
	    {
	    	start_search = false;
	    }

		if(iter > MAX_ITER)
		{
			break;
		}
	}
	std::cout << "Do not find rougt!  " << "iter = " << iter << std::endl;
	std::cout << " len of close list = " << closedList.size() << " len of open list = " << openList.size() << std::endl;
	heuvr_f.clear();
	check_colision_list.clear();
	return false;
}
}
