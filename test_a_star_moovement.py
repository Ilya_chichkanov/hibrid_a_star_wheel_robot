import reeds_shepp
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import dubins
from spline_math import *
from obstacle_maneuvre import *
import pandas as pd
import sys
import os
from celluloid import Camera
sys.path.append(os.getcwd() + "/ReedsSheppPath")
sys.path.append(os.getcwd() + "/HybridAStar")

try:
    from dynamic_programming_heuristic import calc_distance_heuristic
    import reeds_shepp_path_planning as rs
    from car import move, check_car_collision, MAX_STEER, WB, plot_car
    from hybrid_a_star import *
except Exception:
    raise

    

def plot_2d(x_line, y_line):
    fig = plt.figure(figsize = (20,20))
    ax = plt.axes()
    ax.plot(x_line, y_line, 'gray')
    ax.scatter(x_line, y_line, cmap='Greens')
    
def plot_3d(x_line, y_line, z_line):
    fig = plt.figure(figsize = (20,20))
    ax = plt.axes(projection='3d')
    ax.plot3D(x_line, y_line, z_line, 'gray')
    ax.scatter3D(x_line, y_line, z_line, cmap='Greens')
    
def draw_obstacles(ax, obstcl_arr, min_obstcle_avoid_radius, color = 'k'):
    for obstc in obstcl_arr:
        ax.scatter(obstc[0], obstc[1], c = 'k')
        circle1 = plt.Circle((obstc[0], obstc[1]), min_obstcle_avoid_radius, color=color, fill=False)
        ax.add_patch(circle1)


def make_points_from_relative_data(obsracl_realt_list, spline_cfg):
    theta  = np.pi/2
    rot = np.array([[0, -sin(theta), 0 ], [sin(theta), cos(theta), 0],  [0, 0, 1]])
    x0 = spline_point(spline_cfg, 0)
    mov_dir = spline_point_derv(spline_cfg,0)
    dir1_ortgn = np.dot(rot, mov_dir)
    
    mov_dir = mov_dir/np.linalg.norm(mov_dir)
    dir1_ortgn = dir1_ortgn/np.linalg.norm(dir1_ortgn)
    
    
    obstacles_abs = []
    for point in obsracl_realt_list:
        x_new = x0 + point[1]*dir1_ortgn + point[0]*mov_dir
        obstacles_abs.append((x_new[0],x_new[1], 0))
        
    return  obstacles_abs


def make_curve_elem(path):
    c_types = []
    yaw = path.yaw_list
    for ii in range(len(yaw)-1):
        if(path.direction_list[ii+1] == True):
            d = 1
        else:
            d = -1   

        if(abs(yaw[ii+1] - yaw[ii])<0.000001):
            c_types.append((d, "S"))        
        elif( yaw[ii+1]*yaw[ii] < 0):      
            c_types.append(c_types[-1])       
        elif((yaw[ii+1]) > (yaw[ii])):     
            if(d==1):
                mark = "L"
            else:
                mark = "R"                  
            c_types.append((d, mark))           
        elif((yaw[ii+1]) < (yaw[ii])):
            if(d==1):
                mark = "R"
            else:
                mark = "L"               
            c_types.append((d, mark) )

    lenth_cur = 0
    curve_types = []
    points_ind = [0]
    
    for ii in range(len(c_types)-1):
        if(c_types[ii] == c_types[ii+1]):
            lenth_cur += 0.1*c_types[ii][0]
        else:
            lenth_cur += 0.1*c_types[ii][0]
            curve_types.append((round(lenth_cur,2), c_types[ii][1]))
            lenth_cur = 0
            points_ind.append(ii+1)
    
    last_ind = len(c_types) - 1
    if(c_types[last_ind] == c_types[last_ind-1]):                  
        curve_types.append((round(0.1*c_types[last_ind][0]+lenth_cur, 2), c_types[last_ind][1]))         
    else:
        curve_types.append((round(0.1*c_types[last_ind][0], 2), c_types[last_ind][1]))  
        
    points_ind.append(last_ind+1)
    
    return curve_types, c_types, points_ind

def make_obstacle_tree(obstacles_full, visible_radius, start, old_obstacle_set = []):
    ox, oy = [], []
    for obstcl in obstacles_full:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    indexes = obstacle_kd_tree.query_ball_point(start[:2],visible_radius)
    obstacles = set([obstacles_full[ind] for ind in indexes])
    ox, oy = [], []
    if(old_obstacle_set != []):     
        obstacles = obstacles.union(old_obstacle_set)

    for obstcl in obstacles:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)  
    return obstacles, obstacle_kd_tree

def main():
    df = pd.read_csv (os.getcwd()+"/surf_waypoints/car3.csv") 

    x_line = df["x"].to_numpy() 
    y_line = df["y"].to_numpy() 
    z_line = df["z"].to_numpy()

    set_initial = np.hstack((x_line[:, np.newaxis], y_line[:, np.newaxis], 0*z_line[:, np.newaxis]))
    spline = M_spline_from_set(set_initial)
    
    obstacles_full = make_points_from_relative_data([       (0.01, 0.5),
                                                            (3.1, -0.8),
                                                            (1.0,-1.9),
                                                            (1.0, -2.6),
                                                            (-1.0, -5.0),
                                                            (-2,-4),
                                                            (0, -4.7),
                                                            (2,-4),
                                                            (4,-2),
                                                            (0,-0.3),
                                                            (-1.6,1.5),
                                                            (-3,2),
                                                            (-5,-4),
                                                            (1.7,-4.3),
                                                            ( -4, -4),
                                                            (-4.4,2),
                                                            (2.2,-1.2),
                                                            (1.8,-0.5),
                                                          #   (7,-1)
                                                            ], 
                                                            spline[23])
    min_obstcle_avoid_radius = 1.0
    visible_radius = 5

    # Set Initial parameters
    start = [-10.5, 12, np.deg2rad(170.0)]
    border_size = 40
    border = [ [start[0] - border_size , start[0] + border_size], [ start[1] - border_size, start[1] + border_size ] ]
    obstacles, obstacle_kd_tree =  make_obstacle_tree(obstacles_full, visible_radius, start)
    start_ind = 1
    max_dist = 50
    cr_arr = []

    for obtcl in obstacles:
        cr = find_control_params(start_ind, max_dist, spline, obtcl)
        if((cr.ok) and abs(cr.delta)<=1):
            cr.print_all()
            cr_arr.append(cr)

    cr_arr.sort()
    goal_spline_ind = 30 ##??
    if(cr_arr !=[]):
      goal_spline_ind = cr_arr[-1].cfs_index
    
    print("goal ind is ", goal_spline_ind)
    search_ares = [list(spline_find_point_state2d(spline[goal_spline_ind + ind], 0)) for ind in range(3,10)] 
    #cr_arr[-1].print_all()


    print("start : ", start)
    fig = plt.figure(figsize = (10,10))
    camera = Camera(fig)
    ax = plt.axes()

    config = Config(border, XY_GRID_RESOLUTION, YAW_GRID_RESOLUTION)
    
    MAX_COLISION_N = 12
    n_colision = 0
    while(1):
        (path, path_rs_final) = hybrid_a_star_planning(
            start, search_ares, border, obstacle_kd_tree, min_obstcle_avoid_radius, _show_animation_ = False)
        
        path = path[0]
        x = np.array(path.x_list[:])
        y = np.array(path.y_list[:])
        yaw = np.array(path.yaw_list[:])
        length = len(x)
        for ii, (i_x, i_y, i_yaw) in enumerate(zip(x, y, yaw)):
            #plt.cla()
            obstacles, obstacle_kd_tree =  make_obstacle_tree(obstacles_full, visible_radius, [i_x, i_y, i_yaw])
            ax.plot(x_line, y_line, 'gray')
            #ax.scatter(x_line, y_line, cmap='Greens')
            plot_car(i_x, i_y, i_yaw)
            draw_obstacles(ax, obstacles, min_obstcle_avoid_radius)
            plt.plot(x[ii:], y[ii:], "-r", label="Hybrid A* path")
            plt.grid(True)
            plt.axis("equal")
            plt.xlabel("x, m")
            plt.ylabel("y, m")
            plt.draw()
            #plt.show()
            camera.snap()  
            #plt.clf()   
            #plt.pause(0.001) 
            #print(ii)
            if(check_car_collision(x[ii:], y[ii:], yaw[ii:], config, obstacle_kd_tree, 1) == False):
                print("collision, replan parh")
                start = [i_x, i_y, i_yaw]
                n_colision+=1
                break

        if(n_colision == MAX_COLISION_N):
            print("Limit number of collision")
            break

        if(length ==ii+1):
            print("finish")
            break

  
    animation = camera.animate(interval = 200, repeat = True,
                               repeat_delay = 500)
    #Saving the animation
    animation.save('Maneuvre.mp4')
    print("Animation done!!")
    #bact_to_route_elements, _ , elem_p_ind = make_curve_elem(path)
    #print(f"back to route elements = {bact_to_route_elements}")
    #print(path_rs_final.lengths)
    #print(path_rs_final.ctypes)
    #print(path_rs_final.x[0], path_rs_final.y[0], path_rs_final.yaw[0])
    #print(path_rs_final.x[-1], path_rs_final.y[-1], path_rs_final.yaw[-1])
    """
    plt.scatter(path_rs_final.x[0], path_rs_final.y[0], c='r',s = 100)
    plt.scatter(x[elem_p_ind], y[elem_p_ind], c='b',s = 50)
    plt.title(f"Back to route elements = {bact_to_route_elements}")
    """
   
if __name__ == '__main__':
    main()
