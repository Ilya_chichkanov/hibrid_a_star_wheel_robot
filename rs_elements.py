import numpy as np
from numpy import cos, sin
from scipy.spatial.transform import Rotation as Rot

RESOLUTION = 0.01

class RS_element:
    def __init__(self):
        self.type = "s"
        self.start_point = np.array([0,0,0])
        self.length = 0
    def print_all(self):
        print(f"type is {self.type}")
        print(f"length is {self.length}")
        print(f"start point is {self.start_point}")
        
class Path:
    def __init__(self):
        self.u = 0
        self.rs_elements = []
        self.path_points = []

def make_rs_path(resolution, trajectory_types, u_max, start_pose):
    rs_elements = []
    rs_elem_sr_p =  np.copy(start_pose)
    for i in range(len(trajectory_types)):
        elem = trajectory_types[i] 
        rs_elem = RS_element()
        rs_elem.type = elem[0]
        rs_elem.length = elem[1]
        rs_elem.start_point = rs_elem_sr_p          
        rs_elem_sr_p = rs_expand(rs_elem, u_max, 1)     
        rs_elements.append(rs_elem)
    path = Path()
    path.u = u_max
    path.rs_elements = rs_elements
    rs_path_seed(resolution, u_max, path)
    return path

def rs_path_seed(resolution, u_max, path):
    path.path_points = []
    for rs_elem in path.rs_elements:
        l = 0
        s = 0
        while(l<abs(rs_elem.length)):
            s = l/abs(rs_elem.length);
            #print(rs_elem.start_point)
            pose = rs_expand(rs_elem, u_max, s)
            l+=resolution;
            path.path_points.append(pose)
            #print(pose)
    path.path_points = np.array(path.path_points)
    return
  

def rs_expand(rs_element, u_max, s):
    p, dp = rs_p_dp(rs_element, u_max, s)
    fi = np.arctan2(dp[1], dp[0])
    return np.array([p[0], p[1], fi])

def rs_p_dp(rs_element, u_max, s_star):
    x, y, yaw = rs_element.start_point[0], rs_element.start_point[1], rs_element.start_point[2]
    start_point = np.array([x, y])
    rot = Rot.from_euler('z', yaw).as_matrix()[0:2, 0:2]
    p_derv = np.dot(rot, np.array([1, 0]))
    
    if(rs_element.type == 's'):
        p =  start_point + p_derv*rs_element.length*s_star
        dp = p_derv
        return p, dp
    
    dir_ortgn = np.dot(rot,  np.array([0, 1]))
    FI = abs(rs_element.length*u_max)
    
    
    
    if(rs_element.type == 'r'):
        u = u_max
        fi = -FI*s_star*np.sign(rs_element.length)

        
    if(rs_element.type == 'l'):
        u = -u_max
        fi = FI*s_star*np.sign(rs_element.length)
    
    if(u ==0):
        raise
        
    center_point = rs_element.start_point[:2] - dir_ortgn/ u
    rot_new = Rot.from_euler('z', fi).as_matrix()[0:2, 0:2]
    p =  center_point + np.dot(rot_new, dir_ortgn)/ u
    dp = np.dot(rot_new, p_derv)
    return p, dp


#old functions

def pi_2_pi(angle):
    pi = np.pi
    return (angle + pi) % (2 * pi) - pi

def move(x, y, yaw, distance, u):
    x += distance * cos(yaw)
    y += distance * sin(yaw)
    yaw = pi_2_pi(yaw + distance * u)
    return x, y, yaw

def make_trajectory(elem_types, start):
    def make_rs_elem(rs_elem, x, y, yaw):
        elem = RS_element()
        elem.type = rs_elem[0]
        elem.length = rs_elem[1]
        #elem.u = rs_elem[2]
        elem.u = np.abs(rs_elem[2])
        elem.start_point = [x,y, yaw]
        
        if(elem.type =="r"):
            u = - elem.u
        elif(elem.type =="l"):
            u =  elem.u
        else:
            u = 0

        length = 0
        while length <  abs(elem.length):
            x,y, yaw = move(x, y, yaw, RESOLUTION*np.sign(rs_elem[1]), u)
            length+=RESOLUTION
            path_point.append([x,y, yaw ])
        return elem,x,y,yaw
    
    path_point = [start]
    x, y, yaw = start
    trajectory_types = elem_types
    trajectory = Path()
    
    for ii, rs_elem in enumerate(trajectory_types):
        elem,x,y,yaw = make_rs_elem(rs_elem, x, y, yaw)
        trajectory.rs_elements.append(elem)
        
    path_point = np.array(path_point)  
    trajectory.path_points = path_point
    return trajectory

    
def make_smooth_trajectory(elem_types, start):
    def make_rs_elem(rs_elem, x, y, yaw):
        elem = RS_element()
        elem.type = rs_elem[0]
        elem.length = rs_elem[1]
        #elem.u = rs_elem[2]
        elem.u = np.abs(rs_elem[2])
        elem.start_point = [x,y, yaw]
        
        if(elem.type =="r"):
            u = - elem.u
        elif(elem.type =="l"):
            u =  elem.u
        else:
            u = 0

        length = 0
        while length <  abs(elem.length):
            x,y, yaw = move(x, y, yaw, RESOLUTION*np.sign(rs_elem[1]), u)
            length+=RESOLUTION
            path_point.append([x,y, yaw ])
        return elem,x,y,yaw
    
    path_point = [start]
    x, y, yaw = start
    trajectory_types = elem_types
    trajectory = Path()
    
    for ii, rs_elem in enumerate(trajectory_types):
        if(ii ==0):
            elem,x,y,yaw = make_rs_elem(rs_elem, x, y, yaw)
        else:
    
            if(rs_elem[0]!=elem.type and np.sign(rs_elem[1]) == np.sign(elem.length)):
                n = 25
                l = 0.1
                u_list = np.linspace(elem.u, rs_elem[2], n)[1:n-1]
                for u in u_list:
                    if(rs_elem[0] == 's'):
                        type_new = elem.type
                    else:
                        type_new = rs_elem[0] 
                        
                    rs_elem_new = [type_new, np.sign(rs_elem[1])*l, u]
                    elem,x,y,yaw = make_rs_elem(rs_elem_new, x, y, yaw)
                    trajectory.rs_elements.append(elem)

            elem,x,y,yaw = make_rs_elem(rs_elem, x, y, yaw)
            
        trajectory.rs_elements.append(elem)
        
        
    path_point = np.array(path_point)  
    trajectory.path_points = path_point
    return trajectory