import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation as anim
import contextlib
import numpy as np
from spline_math import *
from obstacle_maneuvre import *
import pandas as pd
import sys
import os
import pylab as pl
import random
from enum import Enum
from math import sqrt, cos, sin, tan, pi
sys.path.append(os.getcwd() + "/HybridAStar")


from car import *
from hybrid_a_star import *
from hybrid_a_star import *
from rs_elements import *

MIN_R = 0.33
animation = True   

read_initial_data = True
save_initial_data = True

@contextlib.contextmanager
def get_dummy_context_mgr():
    """
    :return: A dummy context manager for conditionally writing to a movie file.
    """
    yield None

def draw_vector(p, dp): 
    array = np.array([[p[0], p[1], dp[0], dp[1]]])
    X, Y, U, V = zip(*array)
    ax = plt.gca()
    ax.quiver(X, Y, U, V,color='b', angles='xy', scale_units='xy',scale=1.0, width = 0.002, headwidth = 4)


# def check_car_collision_speshial(x_, y_, yaw_, kd_tree, avoid_r = 1.0):
#     ax = plt.axes()
#     for (x, y, yaw) in zip(x_, y_, yaw_):
#         x_s = x + cos(yaw)*WB/4
#         y_s = y + sin(yaw)*WB/4
#         ids = kd_tree.query_ball_point([x_s, y_s], avoid_r+R)
#         if ids:
#             print("VOT", x_s, y_s)

#             draw_obstacles(ax, [[x_s, y_s]], MIN_R, color = 'r')
#             return False
#         x_s = x + cos(yaw)*(2*WB/4)
#         y_s = y + sin(yaw)*(2*WB/4)
#         ids = kd_tree.query_ball_point([x_s, y_s], avoid_r+R)
#         if ids:
#             print("VOT", x_s, y_s)
#             draw_obstacles(ax, [[x_s, y_s]], MIN_R, color = 'r')
#             return False
#         x_s = x + cos(yaw)*(3*WB/4)
#         y_s = y + sin(yaw)*(3*WB/4)
#         ids = kd_tree.query_ball_point([x_s, y_s], avoid_r+R)
#         if ids:
#             draw_obstacles(ax, [[x_s, y_s]], MIN_R, color = 'r')
#             print("VOT", x_s, y_s)
#             return False
#     return True  # no collision

   
def make_obstacle_tree(obstacles_full, visible_radius, start):
    ox, oy = [], []
    for obstcl in obstacles_full:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    indexes = obstacle_kd_tree.query_ball_point(start[:2],visible_radius)
    obstacles = [obstacles_full[ind] for ind in indexes]
    ox, oy = [], []
    for obstcl in obstacles:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    return obstacles, obstacle_kd_tree


def draw_obstacles(ax, obstcl_arr, min_obstcle_avoid_radius, color = 'k'):
    for obstc in obstcl_arr:
        ax.scatter(obstc[0], obstc[1], c = 'k')
        circle1 = plt.Circle((obstc[0], obstc[1]), min_obstcle_avoid_radius, color=color, fill=False)
        ax.add_patch(circle1)

def plot_car(x, y, yaw, robot_ids, status, stop_flag):
    car_color = '-k'
    c, s = cos(yaw), sin(yaw)
    rot = Rot.from_euler('z', -yaw).as_matrix()[0:2, 0:2]
    car_outline_x, car_outline_y = [], []
    for rx, ry in zip(VRX, VRY):
        converted_xy = np.stack([rx, ry]).T @ rot
        car_outline_x.append(converted_xy[0]+x)
        car_outline_y.append(converted_xy[1]+y)

    arrow_x, arrow_y, arrow_yaw = c * 0.5 + x, s * 0.5 + y, yaw
    ax = plt.gca()
    plot_arrow(arrow_x, arrow_y, arrow_yaw)
    plt.plot(car_outline_x, car_outline_y, car_color)
    if(status==Status.OK):
        if(stop_flag):
            color = "orange"
        else:
            color = "green"
    elif(status==Status.ERROR):
        color = "red"
    elif(status==Status.FINISH):
        color = "k"



    plt.text(x, y, str(robot_ids), color=color, fontsize=15)

def generate_points_with_min_distance(n, shape, min_dist):
    # compute grid shape based on number of points
    width_ratio = shape[1] / shape[0]
    num_y = np.int32(np.sqrt(n / width_ratio)) + 1
    num_x = np.int32(n / num_y) + 1

    # create regularly spaced neurons
    x = np.linspace(0., shape[1]-1, num_x, dtype=np.float32)
    y = np.linspace(0., shape[0]-1, num_y, dtype=np.float32)
    coords = np.stack(np.meshgrid(x, y), -1).reshape(-1,2)

    # compute spacing
    init_dist = np.min((x[1]-x[0], y[1]-y[0]))

    # perturb points
    max_movement = (init_dist - min_dist)/2
    noise = np.random.uniform(low=-max_movement,
                                high=max_movement,
                                size=(len(coords), 2))
    coords += noise

    return coords

def make_random_poses(x0_mean, y0_mean, width, min_dist, N):
    coords = generate_points_with_min_distance(n=N, shape=(width,width), min_dist = 4)
    start_mean = np.array([x0_mean, y0_mean])
    for elem in coords:
        elem+=start_mean
        
    angles = np.random.uniform(low=0,
                                high=np.pi,
                                size=(len(coords), 1))
    poses = np.hstack((coords, angles))
    return poses

class Status(Enum):
    OK = 1
    ERROR = 2
    FINISH = 3

class Robot:

    def __init__(self, id_, start, goal, border_size, obstacles, visible_r):
        self.id = id_
        #print(goal, start)
        self.def_priority = 1/np.linalg.norm(np.array(goal)[:2] - np.array(start)[:2])
        print(self.def_priority)
        #a = input()
        self.path = None
        self.cur_pose = start
        self.path_ind = 0
        self.goal = goal
        self.replan_count = 0
        self.plan_ok = True
        self.border_size = border_size
        self.visible_r = visible_r
        self.update_obstacle_configuration(obstacles)
        self.speed = 1
        self.car_length = 0.75
        self.car_r = 0.4
        self.forced_stop = False
        self.masters_list_indexes = []
        self.colision_list = []
        self.status = Status.OK


    def get_center_point(self):
        x = self.cur_pose[0]
        y = self.cur_pose[1]
        yaw = self.cur_pose[2]
        return np.array([x + self.car_length/2*np.cos(yaw), y + self.car_length/2*np.sin(yaw)])

    def check_colision_with_other_robot(self, robot, min_dist):
        distance_to_goal = len(self.path.x_list) - self.path_ind
        distance_to_goal_other = len(robot.path.x_list) - robot.path_ind
        for i in range(max(distance_to_goal_other,distance_to_goal)):
            if(i>50):
                break
            p1 = np.array(self.path.x_list[self.path_ind+i], self.path.y_list[self.path_ind+i])
            p2 = np.array(robot.path.x_list[robot.path_ind+i], robot.path.y_list[robot.path_ind+i])
            if(np.linalg.norm(p1-p2)<=min_dist):
                return True
        return False

    def get_obstcle_points(self):
        x = self.cur_pose[0]
        y = self.cur_pose[1]
        yaw = self.cur_pose[2]
        # rot = Rot.from_euler('z', yaw).as_matrix()[0:2, 0:2]
        # dir = rot@np.array([1, 0])
        # print(self.id, yaw)
        s = sin(yaw)
        c = cos(yaw)

        points =  np.array([
            [x + self.car_length*c/4, y + self.car_length*s/4],
            [x + 2*self.car_length*c/4, y + 2*self.car_length*s/4],
            [x + 3*self.car_length*c/4, y + 3*self.car_length*s/4]])

        return points

    def update_obstacle_configuration(self, obstacles):
        _, visible_obstacle_tree = make_obstacle_tree(obstacles, self.visible_r, self.cur_pose)
        self.visible_obstacle_tree = visible_obstacle_tree

    def move(self, obstacles):
        if(self.status == Status.FINISH):
            return self.status

        if(self.status ==  Status.OK):
            if((self.path_ind >= len(self.path.x_list)-1-self.speed)and(self.status!=Status.FINISH)):
                print(f"Robot{self.id} finished way")
                self.status = Status.FINISH
                return self.status

        print(self.id, self.status, self.forced_stop, self.masters_list_indexes)
        if((self.status == Status.OK) and (self.forced_stop == False)):
            self.update_obstacle_configuration(obstacles)
            indexes = self.visible_obstacle_tree.query_ball_point(self.goal[:2], 1.5)  
            if(len(indexes)>0):
                self.status = Status.ERROR
                return self.status
            if(self.path.cost > 1e5):
                offset = 6
                print("cost", self.path.cost)     
            else:
                offset = 0

            if(check_car_collision(self.path.x_list[self.path_ind+offset:self.path_ind+offset+100*self.visible_r], 
                self.path.y_list[self.path_ind+offset:self.path_ind+offset+100*self.visible_r], 
                self.path.yaw_list[self.path_ind+offset:self.path_ind+offset+100*self.visible_r], 
                self.visible_obstacle_tree, MIN_R - 0.1) == False):   
                print(self.id, "colision")    
                self.status = self.replan()             
            else:
                self.path_ind += self.speed 
                self.cur_pose = [self.path.x_list[self.path_ind], self.path.y_list[self.path_ind], self.path.yaw_list[self.path_ind]]
                print(self.id, "move")
                self.status =  Status.OK  

        return self.status
                    
    def replan(self):
        print(f"Robot{self.id} replan way") 
        self.replan_count+=1
        border = [[self.cur_pose[0] - self.border_size, self.cur_pose[0] + self.border_size], 
               [self.cur_pose[1] - self.border_size, self.cur_pose[1] + self.border_size]]   
        path = hybrid_a_star_planning(
                    self.cur_pose, [self.goal], 
                    border, self.visible_obstacle_tree, 
                    MIN_R)

        if(path!=None):
            self.path = path
            self.path_ind = 0
            if(self.path.cost > 1e5):
                offset = 6
                print("cost > 1e5", self.path.cost)     
            else:
                offset = 0
                print("cost < 1e5", self.path.cost)
            if(check_car_collision(self.path.x_list[self.path_ind+offset:self.path_ind+offset+10*self.visible_r], 
                self.path.y_list[self.path_ind+offset:self.path_ind+offset+10*self.visible_r], 
                self.path.yaw_list[self.path_ind+offset:self.path_ind+offset+10*self.visible_r], 
                self.visible_obstacle_tree, MIN_R) == False): 
                #plt.plot()  
                print(self.id, "syka")    
                a = input()
                #raise
            return Status.OK   
        else:
            print(f"{self.id} Replan failed")
            return Status.ERROR   
           
            

    def plot(self):
        plot_car(self.cur_pose[0], self.cur_pose[1], self.cur_pose[2], self.id, self.status, self.forced_stop)
        if(self.status==Status.OK):
            plt.plot(self.path.x_list[self.path_ind:], self.path.y_list[self.path_ind:], c = 'r')
        
class Robot_envieroment():
    def __init__(self, robot_list, obstacles_conf, obstcl_r, stop_radius):
        self.robot_list = robot_list
        self.obstacl_r = obstcl_r
        self.obstacles_conf = obstacles_conf
        self.stop_radius = stop_radius  
        self.N = len(self.robot_list)
        for i, robot in enumerate(robot_list):
            obstacles = self.convert_robots_pos_to_obstcl(i)
            robot.update_obstacle_configuration(obstacles)
            robot.status = robot.replan()
            
    def show(self, ax):
        draw_obstacles(ax, self.obstacles_conf, self.obstacl_r)
        for i, robot in enumerate(self.robot_list):
            robot.plot()
        robot_pos_list = self.get_robots_pos()
        for i in range(self.N):
            #qwer = self.convert_robots_pos_to_obstcl(i)
            #draw_obstacles(ax, qwer, self.obstacl_r)
            x_i = robot_pos_list[i]
            for j in self.robot_list[i].masters_list_indexes:
                x_q = robot_pos_list[j]
                dist = np.linalg.norm(x_q - x_i)
                draw_vector(x_q, -(x_q - x_i))
             
    def get_robots_pos(self):
        robot_pos_list = []
        for i, robot in enumerate(self.robot_list):
            robot_pos_list.append(robot.get_center_point())
        robot_pos_list = np.array(robot_pos_list)
        return robot_pos_list

    def convert_robots_pos_to_obstcl(self, n): #n is robot index
        robots_obstcl_points = []
        for i in range(self.N):
            if(i==n):
                continue
            if(len(robots_obstcl_points)==0):
                robots_obstcl_points = self.robot_list[i].get_obstcle_points()
            else:
                robots_obstcl_points = np.vstack((robots_obstcl_points, self.robot_list[i].get_obstcle_points()))
        obstacles = np.vstack((self.obstacles_conf, robots_obstcl_points))
        return obstacles

    def calc_penalty(self, status):
        penalty=0
        if(status==Status.ERROR):
            penalty += 1e6
        elif(status==Status.FINISH):
            penalty += 2e6
        return penalty

    def move(self):
        robot_colision_list =  [[]]*len(self.robot_list)      
        robot_pos_list = self.get_robots_pos()
        robot_position_tree = cKDTree(robot_pos_list)     
        robot_status_list = [robot.status for robot in self.robot_list]
        for i, robot_pos in enumerate(robot_pos_list):
            indexes = robot_position_tree.query_ball_point(robot_pos, self.stop_radius) 
            indexes.remove(i)
            robot_colision_list[i] = [(ind, robot_status_list[ind]) for ind in indexes]
    
        for i, robot in enumerate(self.robot_list):
            neigbour_status_list = []
            if(set(robot_colision_list[i]) != set(robot.colision_list)):
                print("situation changed") 
                if(robot.status == Status.ERROR):
                    obstacles = self.convert_robots_pos_to_obstcl(i)
                    robot.update_obstacle_configuration(obstacles)
                    robot.status = robot.replan()

                penalty = robot.def_priority + self.calc_penalty(robot.status)

                master_indexes = []
                for elem in robot_colision_list[i]:     
                    ind = elem[0]     
                    penalty_neigbour = self.robot_list[ind].def_priority +  self.calc_penalty(self.robot_list[ind].status)
                    if(penalty_neigbour < penalty): 
                        master_indexes.append(ind)

                robot.colision_list = robot_colision_list[i]

                if(len(master_indexes) > 0):       
                    print("set")
                    robot.forced_stop = False
                    for ind in master_indexes:
                        if(robot.status == Status.OK and self.robot_list[ind].status == Status.OK):
                            if(robot.check_colision_with_other_robot(self.robot_list[ind],1)):
                                robot.forced_stop = True
                                break
                    robot.masters_list_indexes = master_indexes   
                else:
                    robot.forced_stop = False
                    robot.masters_list_indexes = []   
        

        for i in range(self.N):
            print(self.robot_list[i].forced_stop, end = ' ')
        print()
        finish_list = np.array([Status.OK]*len(self.robot_list))

        for i, robot in enumerate(self.robot_list):
            obstacles = self.convert_robots_pos_to_obstcl(i)
            finish_list[i] = robot.move(obstacles)
        return np.all(finish_list == Status.FINISH)


#np.random.seed(42)
obstacles_full = [  [0.01, 0.5],
                    [3.1, -0.8],
                    [1.0,-1.9],
                    [1.0, -2.6],
                    [-1.0, -5.0],
                    [-2,-4],
                    [0, -4.7],
                    [2,-4],
                    [4,-2],
                    [0,-0.3],
                    [-1.6,1.5],
                    [-3,2],
                    [-5,-4],
                    [1.7,-4.3],
                    [ -4, -4],
                    [-4.4,2],
                    [2.2,-1.2],
                    [1.8,-0.5]
                    ]

obstacles_full = np.array(obstacles_full)[:0]                                          
border_size = 100

print("start")
fig = plt.figure(figsize = (20,10))
if(animation==True): 
    get_ff_mpeg_writer = anim.writers['ffmpeg']
    movie_writer = get_ff_mpeg_writer(fps=15, metadata=None)
    
ax = plt.axes()

ii=0
border_size = 100

goal1 = [-10.5, 12, np.deg2rad(170.0)]
start1 = [0.0, -15, np.deg2rad(70.0)]

robot1 = Robot(1, start1, goal1, border_size, obstacles_full, 4)


start2 = [-10, -10, np.deg2rad(45.0)]
goal2 =  [5, 7, np.deg2rad(100.0)]
robot2 = Robot(0, start2, goal2, border_size, obstacles_full, 4)

start3 = [-4, -9, np.deg2rad(0.0)]
goal3 =  [-2, 8, np.deg2rad(29.0)]
robot3 = Robot(2, start3, goal3, border_size, obstacles_full, 4)

start4 =  [-5, 15, np.deg2rad(130.0)]
goal4 = [5, -5, np.deg2rad(0.0)]
robot4 = Robot(3, start4, goal4, border_size, obstacles_full, 4)

start5 =  [-5, 13, np.deg2rad(130.0)]
goal5 = [-5, -13, np.deg2rad(20.0)]
robot5 = Robot(4, start5, goal5, border_size, obstacles_full, 4)

start6 =  [-8, 13, np.deg2rad(190.0)]
goal6 = [-7, -16, np.deg2rad(40.0)]
robot6 = Robot(5, start6, goal6, border_size, obstacles_full, 4)

robot_list = [robot1, robot2, robot3, robot4, robot5,robot6]

N = 9
startx0 = 0
starty0 = -25
start_width = 5
start_min_dist = 1.4


goalx0 = -20
goaly0 = 0
goal_width = 10
goal_min_dist = 15

start_poses = make_random_poses(startx0, starty0, start_width, start_min_dist, N)
#goal_poses = make_random_poses(goalx0, goaly0, goal_width, goal_min_dist, N)

goal_poses = np.array([ [-20, -10, np.deg2rad(90.0)],
                        [-15, -10, np.deg2rad(90.0)],
                        [-10, -10, np.deg2rad(90.0)],
                        [-5, -10, np.deg2rad(90.0)],
                        [0, -10, np.deg2rad(90.0)],
                        [5, -10, np.deg2rad(90.0)],
                        [10,-10, np.deg2rad(90.0)],
                        [15,-10, np.deg2rad(90.0)],
                        [20,-10, np.deg2rad(90.0)],
                         [20,-5, np.deg2rad(90.0)],
                          [15,-5, np.deg2rad(90.0)],
                           [10,-5, np.deg2rad(90.0)]])

robot_ids = np.arange(0,len(start_poses))
#random.shuffle(robot_ids)

#goal1 = [-10.5, 12, np.deg2rad(170.0)]
#start1 = [0.0, -15, np.deg2rad(70.0)]
#robot1 = Robot(1, start1, goal1, border_size, obstacles_full, 7)
#
#
#start2 = [-10, -10, np.deg2rad(45.0)]
#goal2 =  [5, 7, np.deg2rad(100.0)]
#robot2 = Robot(0, start2, goal2, border_size, obstacles_full, 7)
#start_poses = [start1, start2]
#goal_poses = [goal1, goal2]
#robot_list = []
#robot_ids = np.arange(0,2)

if(read_initial_data):  
    with open('initial_car_poses.npy', 'rb') as f:
        start_poses = np.load(f)
        #robot_ids = np.load(f) 

if(save_initial_data):  
   with open('initial_car_poses.npy', 'wb') as f:
    np.save(f, start_poses)
    np.save(f, robot_ids)

robot_list = []
Robot_visible_r = 5
for i in range(len(start_poses)):
    #if(i==)
    start = np.array(start_poses[i])  
    goal = np.array(goal_poses[i])
    robot = Robot(i, start, goal, border_size, obstacles_full, Robot_visible_r)
    robot_list.append(robot)





#robot_ids = [0, 5, 3, 2, 1, 4]
for i, robot in enumerate(robot_list):
    robot.id = robot_ids[i]

print(f"robot_ids is {robot_ids}")
robor_env = Robot_envieroment(robot_list, obstacles_full, MIN_R, 3)
with movie_writer.saving(fig, "test.mp4", dpi=100) if animation else get_dummy_context_mgr():
    while(1):  
        
        if(animation==True):    
            movie_writer.grab_frame()
        plt.pause(0.001) 
        plt.cla()
        robor_env.show(ax)

        plt.grid(True)
        plt.axis("equal")
        plt.xlabel("x, m")
        plt.ylabel("y, m")
        plt.ylim(-30, -5)
        plt.xlim(-15, 15)
        plt.draw()  

        #a = input()
        stop = robor_env.move()
        if(stop):
            break

        ii +=1
        if(ii==500):
            break
        #print(ii)



print(f"Simulation take {ii} iteration!")

a = input()
