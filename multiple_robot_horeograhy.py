import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation as anim
import contextlib
import numpy as np
from spline_math import *
from obstacle_maneuvre import *
import pandas as pd
import sys
import os
import pylab as pl
import random

sys.path.append(os.getcwd() + "/ReedsSheppPath")
sys.path.append(os.getcwd() + "/HybridAStar")


from car import *
from hybrid_a_star import *
from hybrid_a_star import *
from rs_elements import *

MIN_R = 1.0
animation = True   

read_initial_data = False
save_initial_data = True

@contextlib.contextmanager
def get_dummy_context_mgr():
    """
    :return: A dummy context manager for conditionally writing to a movie file.
    """
    yield None

def make_obstacle_tree(obstacles_full, visible_radius, start):
    ox, oy = [], []
    for obstcl in obstacles_full:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    indexes = obstacle_kd_tree.query_ball_point(start[:2],visible_radius)
    obstacles = [obstacles_full[ind] for ind in indexes]
    ox, oy = [], []
    for obstcl in obstacles:
        ox.append(obstcl[0])
        oy.append(obstcl[1])
    tox, toy = ox[:], oy[:]
    obstacle_kd_tree = cKDTree(np.vstack((tox, toy)).T)
    return obstacles, obstacle_kd_tree

#def make_obstacle_tree(obstacles_full, visible_radius, start):
#    obstacle_kd_tree = cKDTree(obstacles_full)
#    indexes = obstacle_kd_tree.query_ball_point(start[:2],visible_radius)
#    obstacles_visible = np.array([obstacles_full[ind] for ind in indexes])
#    obstacle_kd_tree_visible = cKDTree(obstacles_visible)
#    return obstacles_visible, obstacle_kd_tree_visible
#


def draw_obstacles(ax, obstcl_arr, min_obstcle_avoid_radius, color = 'k'):
    for obstc in obstcl_arr:
        ax.scatter(obstc[0], obstc[1], c = 'k')
        circle1 = plt.Circle((obstc[0], obstc[1]), min_obstcle_avoid_radius, color=color, fill=False)
        ax.add_patch(circle1)

def plot_car(x, y, yaw, robot_ids):
    car_color = '-k'
    c, s = cos(yaw), sin(yaw)
    rot = Rot.from_euler('z', -yaw).as_matrix()[0:2, 0:2]
    car_outline_x, car_outline_y = [], []
    for rx, ry in zip(VRX, VRY):
        converted_xy = np.stack([rx, ry]).T @ rot
        car_outline_x.append(converted_xy[0]+x)
        car_outline_y.append(converted_xy[1]+y)

    arrow_x, arrow_y, arrow_yaw = c * 0.5 + x, s * 0.5 + y, yaw
    ax = plt.gca()
    plot_arrow(arrow_x, arrow_y, arrow_yaw)
    plt.plot(car_outline_x, car_outline_y, car_color)
    pl.text(x, y, str(robot_ids), color="red", fontsize=12)

def generate_points_with_min_distance(n, shape, min_dist):
    # compute grid shape based on number of points
    width_ratio = shape[1] / shape[0]
    num_y = np.int32(np.sqrt(n / width_ratio)) + 1
    num_x = np.int32(n / num_y) + 1

    # create regularly spaced neurons
    x = np.linspace(0., shape[1]-1, num_x, dtype=np.float32)
    y = np.linspace(0., shape[0]-1, num_y, dtype=np.float32)
    coords = np.stack(np.meshgrid(x, y), -1).reshape(-1,2)

    # compute spacing
    init_dist = np.min((x[1]-x[0], y[1]-y[0]))

    # perturb points
    max_movement = (init_dist - min_dist)/2
    noise = np.random.uniform(low=-max_movement,
                                high=max_movement,
                                size=(len(coords), 2))
    coords += noise

    return coords

def make_random_poses(x0_mean, y0_mean, width, min_dist, N):
    coords = generate_points_with_min_distance(n=N, shape=(width,width), min_dist = 4)
    start_mean = np.array([x0_mean, y0_mean])
    for elem in coords:
        elem+=start_mean
        
    angles = np.random.uniform(low=0,
                                high=np.pi,
                                size=(len(coords), 1))
    poses = np.hstack((coords, angles))
    return poses


class Robot:
    def __init__(self, id_, start, goal, border_size, obstacles, visible_r):
        self.id = id_
        self.path = None
        self.cur_pose = start
        self.path_ind = 0
        self.goal = goal
        self.replan_count = 0
        self.border_size = border_size
        self.visible_r = visible_r
        self.is_moving = True
        self.update_obstacle_configuration(obstacles)
        self.speed = 1
        self.car_length = 0.75
        self.forced_stop = False

    def get_center_point(self):
        x = self.cur_pose[0]
        y = self.cur_pose[1]
        yaw = self.cur_pose[2]
        return np.array([x + self.car_length/2*np.cos(yaw), y + self.car_length/2*np.sin(yaw)])

    def update_obstacle_configuration(self, obstacles):
        _, visible_obstacle_tree = make_obstacle_tree(obstacles, self.visible_r, self.cur_pose)
        self.visible_obstacle_tree = visible_obstacle_tree
        self.border = [[self.cur_pose[0] - self.border_size, self.cur_pose[0] + self.border_size], 
                       [self.cur_pose[1] - self.border_size, self.cur_pose[1] + self.border_size]]   

    def move(self, obstacles):
        if((self.path_ind >= len(self.path.x_list)-1-self.speed)and(self.is_moving)):
            print(f"Robot{self.id} finished way")
            self.is_moving = False
    
        if(self.is_moving):
            if(self.forced_stop == False):
                self.path_ind+=self.speed
                self.cur_pose = [self.path.x_list[self.path_ind], self.path.y_list[self.path_ind], self.path.yaw_list[self.path_ind]]
                self.update_obstacle_configuration(obstacles)
                if(check_car_collision(self.path.x_list[self.path_ind:], 
                                       self.path.y_list[self.path_ind:], 
                                       self.path.yaw_list[self.path_ind:], 
                                       self.visible_obstacle_tree) == False):
                    print(f"Robot{self.id} replan way")  
                    self.replan()
        return not self.is_moving
                    
    def replan(self):
        self.replan_count+=1
        path = hybrid_a_star_planning(
                    self.cur_pose, [self.goal], 
                    self.border, self.visible_obstacle_tree, 
                    MIN_R)
        if(path!=None):
            self.path = path
            self.path_ind = 0
        else:
            self.forced_stop = True
            print(f"{self.id} AAAAAAAAAAAAAAAAA")
        #return self.path

    def plot(self):
        plot_car(self.cur_pose[0], self.cur_pose[1], self.cur_pose[2], self.id)
        plt.plot(self.path.x_list[self.path_ind:], self.path.y_list[self.path_ind:], c = 'r')
        
class Robot_envieroment():
    def __init__(self, robot_list, obstacles_conf, obstcl_r, stop_radius):
        self.robot_list = robot_list
        self.obstacl_r = obstcl_r
        self.obstacles_conf = obstacles_conf
        self.stop_radius = stop_radius  
        self.N = len(self.robot_list)
        for robot in robot_list:
            robot.replan()
            
    def show(self, ax):
        draw_obstacles(ax, self.obstacles_conf, self.obstacl_r)
        for robot in self.robot_list:
            robot.plot()
            
    def move(self):
        finish_list = np.array([False]*len(self.robot_list))
        robot_colision_list =  [[]]*len(self.robot_list)
        robot_goal_not_free_list = np.array([False]*len(self.robot_list))
        robot_stop_list = np.array([False]*len(self.robot_list))
        robot_ids_list = np.array([0]*len(self.robot_list))
        robot_pos_list = []

        for i, robot in enumerate(self.robot_list):
            robot_pos_list.append(robot.get_center_point())
        robot_pos_list = np.array(robot_pos_list)

        robot_position_tree = cKDTree(robot_pos_list)
        
        indexes = []
        for i, robot_pos in enumerate(robot_pos_list):
            indexes = robot_position_tree.query_ball_point(robot_pos, self.stop_radius) 
            indexes.remove(i)
            robot_colision_list[i] = indexes
            if(len(indexes) > 0):
                #confdl_id = [self.robot_list[ind].id for ind in indexes]
                #print(f"Robots {confdl_id} have conflict")
                for ind in indexes:
                    if(np.linalg.norm(robot_pos_list[ind] - self.robot_list[i].goal[:2]) < 2.0):
                        robot_goal_not_free_list[i] = True
            stop = not self.robot_list[i].is_moving
            robot_ids_list[i] = self.N*self.N*robot_goal_not_free_list[i] + self.N*len(robot_colision_list[i]) + self.robot_list[i].id + self.N*self.N*self.N*stop

        for i in range(self.N):
            if(robot_ids_list[i] > self.N*self.N):
                robot_stop_list[i] = True
                #print(i,"wefwfwfwfwerfw")
                continue
            if(len(robot_colision_list[i]) > 0):
                for ind in robot_colision_list[i]: 
                    if(robot_ids_list[ind] < robot_ids_list[i]):
                       robot_stop_list[i] = True        
                    if(robot_ids_list[ind] == robot_ids_list[i]):
                        raise
        #print(robot_colision_list)
        #print(robot_stop_list)              
        for i, robot in enumerate(self.robot_list):
            new_obstacles = self.obstacles_conf
            if(robot_stop_list[i] == False):
                robot.forced_stop = False

            else:
                robot.forced_stop = True

            if(len(robot_colision_list[i])>0):
                new_obstacles = np.array(robot_pos_list[robot_colision_list[i]])
                if(len(new_obstacles)>0):
                    new_obstacles = np.vstack((self.obstacles_conf, new_obstacles))

            finish_list[i] = robot.move(new_obstacles)
        return np.all(finish_list == True)


#np.random.seed(42)
obstacles_full = [  [0.01, 0.5],
                    [3.1, -0.8],
                    [1.0,-1.9],
                    [1.0, -2.6],
                    [-1.0, -5.0],
                    [-2,-4],
                    [0, -4.7],
                    [2,-4],
                    [4,-2],
                    [0,-0.3],
                    [-1.6,1.5],
                    [-3,2],
                    [-5,-4],
                    [1.7,-4.3],
                    [ -4, -4],
                    [-4.4,2],
                    [2.2,-1.2],
                    [1.8,-0.5]
                    ]

obstacles_full = np.array(obstacles_full)[:0]                                          
border_size = 100

print("start")
fig = plt.figure(figsize = (10,10))
if(animation==True): 
    get_ff_mpeg_writer = anim.writers['ffmpeg']
    movie_writer = get_ff_mpeg_writer(fps=15, metadata=None)
    
ax = plt.axes()

ii=0
border_size = 100

goal1 = [-10.5, 12, np.deg2rad(170.0)]
start1 = [0.0, -15, np.deg2rad(70.0)]
robot1 = Robot(1, start1, goal1, border_size, obstacles_full, 7)


start2 = [-10, -10, np.deg2rad(45.0)]
goal2 =  [5, 7, np.deg2rad(100.0)]
robot2 = Robot(0, start2, goal2, border_size, obstacles_full, 7)

start3 = [-4, -9, np.deg2rad(0.0)]
goal3 =  [-2, 8, np.deg2rad(29.0)]
robot3 = Robot(2, start3, goal3, border_size, obstacles_full, 7)

start4 =  [-5, 15, np.deg2rad(130.0)]
goal4 = [5, -5, np.deg2rad(0.0)]
robot4 = Robot(3, start4, goal4, border_size, obstacles_full, 7)

start5 =  [-5, 13, np.deg2rad(130.0)]
goal5 = [-5, -13, np.deg2rad(20.0)]
robot5 = Robot(4, start5, goal5, border_size, obstacles_full, 7)

start6 =  [-8, 13, np.deg2rad(190.0)]
goal6 = [-7, -16, np.deg2rad(40.0)]
robot6 = Robot(5, start6, goal6, border_size, obstacles_full, 7)

robot_list = [robot1, robot2, robot3, robot4, robot5,robot6]

N = 4
startx0 = 0
starty0 = 10
start_width = 7
start_min_dist = 2


goalx0 = -20
goaly0 = 0
goal_width = 10
goal_min_dist = 15

start_poses = make_random_poses(startx0, starty0, start_width, start_min_dist, N)
goal_poses = make_random_poses(goalx0, goaly0, goal_width, goal_min_dist, N)

goal_poses = np.array([[-20, 0, np.deg2rad(90.0)],
                        [-15, 0, np.deg2rad(90.0)],
                        [-10, 0, np.deg2rad(90.0)],
                        [-5, 0, np.deg2rad(90.0)],
                        [0, 0, np.deg2rad(90.0)],
                        [5,0, np.deg2rad(90.0)]])
robot_ids = np.arange(0,len(robot_list))
random.shuffle(robot_ids)

#goal1 = [-10.5, 12, np.deg2rad(170.0)]
#start1 = [0.0, -15, np.deg2rad(70.0)]
#robot1 = Robot(1, start1, goal1, border_size, obstacles_full, 7)
#
#
#start2 = [-10, -10, np.deg2rad(45.0)]
#goal2 =  [5, 7, np.deg2rad(100.0)]
#robot2 = Robot(0, start2, goal2, border_size, obstacles_full, 7)
#start_poses = [start1, start2]
#goal_poses = [goal1, goal2]
#robot_list = []
#robot_ids = np.arange(0,2)

if(read_initial_data):  
    with open('initial_car_poses_bag.npy', 'rb') as f:
        start_poses = np.load(f)
        robot_ids = np.load(f) 


robot_list = []
for i in range(len(start_poses)):
    start = list(start_poses[i])  
    goal = list(goal_poses[i])
    robot = Robot(i, start, goal, border_size, obstacles_full, 7)
    robot_list.append(robot)





#robot_ids = [0, 5, 3, 2, 1, 4]
for i, robot in enumerate(robot_list):
    robot.id = robot_ids[i]

print(f"robot_ids is {robot_ids}")
robor_env = Robot_envieroment(robot_list, obstacles_full, MIN_R, 3)
with movie_writer.saving(fig, "test.mp4", dpi=100) if animation else get_dummy_context_mgr():
    while(1):  
        
        if(animation==True):    
            movie_writer.grab_frame()
        plt.pause(0.001) 
        plt.cla()
        robor_env.show(ax)

        plt.grid(True)
        plt.axis("equal")
        plt.xlabel("x, m")
        plt.ylabel("y, m")
        plt.draw()  
        
        stop = robor_env.move()
        if(stop):
            break

        ii +=1
        #print(ii)

if(save_initial_data):  
   with open('initial_car_poses.npy', 'wb') as f:
    np.save(f, start_poses)
    np.save(f, robot_ids)


print(f"Simulation take {ii} iteration!")

a = input()
